import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hive/hive.dart';

import 'package:wibuapp/Controller/AuthController.dart';
import 'package:wibuapp/Model/safeContact.dart';
import 'package:wibuapp/Model/user.dart';

Box<SafeContact> box;

class CrudMethodSafeContacts {
  final Firestore rtdb = Firestore.instance;
  var auth = new AuthController();

  Future<User> getSafeContacts(User user) async {
    if (auth.isLoggedin()) {
      await clearSafeContactLocalStorage();

      final QuerySnapshot querySnapshot = await rtdb
          .collection('users')
          .document(user.id)
          .collection('contacts')
          .getDocuments();
      final List docs = querySnapshot.documents;
      print(docs.length);
      box = await Hive.openBox<SafeContact>('safeContact');
      for (var d in docs) {
        final SafeContact safeContact = new SafeContact();
        safeContact.phone = d.data['phone'];
        safeContact.name = d.data['name'];
        safeContact.avatar = d.data['avatar'] != null
            ? new List<int>.from(d.data()['avatar'])
            : d.data['avatar'];
        safeContact.active = d.data['active'];
        await box.put(safeContact.phone, safeContact);
        user.addSafeContact(safeContact);
      }
      print('lcorrectly');
      return user;
    }
    return null;
  }

  Future<User> addSafeContact(User user, SafeContact safeContact) async {
    final Map<String, dynamic> data = {
      'phone': safeContact.phone,
      'name': safeContact.name,
      'avatar': safeContact.avatar,
      'active': safeContact.active,
    };
    if (auth.isLoggedin()) {
      await rtdb
          .collection('users')
          .document(user.id)
          .collection('contacts')
          .document(safeContact.phone)
          .setData(data);

      await box.put(safeContact.phone, safeContact);
      user.addSafeContact(safeContact);
      return user;
    }
    return null;
  }

  Future updateSafeContact(
      User user, SafeContact safeContact, SafeContact newData) async {
    final Map<String, dynamic> data = {
      'phone': newData.phone,
      'name': newData.name,
      'avatar': newData.avatar,
      'active': newData.active,
    };
    if (auth.isLoggedin()) {
      if (safeContact.phone != newData.phone) {
        final batch = rtdb.batch();
        var document = rtdb
            .collection('users')
            .document(user.id)
            .collection('contacts')
            .document(safeContact.phone);
        batch.delete(document);
        document = rtdb
            .collection('users')
            .document(user.id)
            .collection('contacts')
            .document(newData.phone);
        batch.updateData(document, data);
        await batch.commit();
        await box.delete(safeContact.phone);
        await box.put(newData.phone, newData);
      } else {
        rtdb
            .collection('users')
            .document(user.id)
            .collection('contacts')
            .document(safeContact.phone)
            .updateData(data);
        await safeContact.save();
      }
      user.updateSafeContact(safeContact, newData);
      return user;
    }
    return null;
  }

  Future deleteSafeContact(User user, SafeContact safeContact) async {
    if (auth.isLoggedin()) {
      await rtdb
          .collection('users')
          .document(user.id)
          .collection('contacts')
          .document(safeContact.phone)
          .delete();
      await box.delete(safeContact.phone);
      user.deleteSafeContact(safeContact);
      return user;
    }
    return null;
  }

  Future<List<SafeContact>> getSafeContactsLocalStorage() async {
    if (auth.isLoggedin()) {
      final List<SafeContact> safeContacts = box.values.toList();
      return safeContacts;
    }
    return null;
  }

  Future<void> clearSafeContactLocalStorage() async {
    final box = await Hive.openBox<SafeContact>('safeContact');
    await box.deleteFromDisk();
  }
}
