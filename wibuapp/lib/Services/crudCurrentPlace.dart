import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hive/hive.dart';

import 'package:wibuapp/Controller/AuthController.dart';
import 'package:wibuapp/Model/currentPlace.dart';
import 'package:wibuapp/Model/user.dart';

class CrudMethodCurrentPlaces {
  final Firestore rtdb = Firestore.instance;
  var auth = new AuthController();

  Future<User> getCurrentPlaces(User user) async {
    if (auth.isLoggedin()) {
      await clearCurrentPlaceLocalStorage();

      final QuerySnapshot querySnapshot = await rtdb
          .collection('users')
          .document(user.id)
          .collection('currentPlace')
          .getDocuments();
      final docs = querySnapshot.documents;
      print('currentPlace');
      final box = await Hive.openBox<CurrentPlace>('currentPlace');

      print('currentPlaceAll');
      print(box.length);
      for (var d in docs) {
        print(d.data);
        final CurrentPlace currentPlace = new CurrentPlace();
        currentPlace.address = d.data['address'];
        currentPlace.latitude = d.data['latitude'];
        currentPlace.longitude = d.data['longitude'];
        currentPlace.timesvisited = d.data['timesvisited'];
        currentPlace.addedTosafeplace = d.data['addedTosafeplace'];
        currentPlace.lstVisited = d.data['lstVisited'] != null
            ? DateTime.fromMicrosecondsSinceEpoch(
                d.data['lstVisited'].microsecondsSinceEpoch)
            : DateTime.now();

        await box.put(
            currentPlace.address.codeUnitAt(0).toString(), currentPlace);
        user.addCurrentPlace(currentPlace);
      }
      box.close().onError((error, stackTrace) => null);
      return user;
    }
    return null;
  }

  Future<User> addCurrentPlace(User user, CurrentPlace currentPlace) async {
    final Map<String, dynamic> data = {
      'address': currentPlace.address,
      'latitude': currentPlace.latitude,
      'longitude': currentPlace.longitude,
      'timesvisited': currentPlace.timesvisited,
      'addedTosafeplace': currentPlace.addedTosafeplace,
      'lstVisited': currentPlace.lstVisited,
    };
    if (auth.isLoggedin()) {
      await rtdb
          .collection('users')
          .document(user.id)
          .collection('currentPlace')
          .document(currentPlace.address)
          .setData(data);
      final box = await Hive.openBox<CurrentPlace>('currentPlace');
      await box.put(
          currentPlace.address.codeUnitAt(0).toString(), currentPlace);
      if (box.isOpen) {
        box.close();
      }
      user.addCurrentPlace(currentPlace);
      return user;
    }
    return null;
  }

  Future<List<CurrentPlace>> getCurrentPlaceLocalStorage() async {
    if (auth.isLoggedin()) {
      final box = await Hive.openBox<CurrentPlace>('currentPlace');
      final List<CurrentPlace> safePlaces = box.values.toList();
      if (box.isOpen) {
        box.close();
      }

      return safePlaces;
    }
    return null;
  }

  Future<User> updateCurrentPlace(
      User user, CurrentPlace safePlace, CurrentPlace newData) async {
    final Map<String, dynamic> data = {
      'address': newData.address,
      'latitude': newData.latitude,
      'longitude': newData.longitude,
      'timesvisited': newData.timesvisited,
      'addedTosafeplace': newData.addedTosafeplace,
      'lstVisited': newData.lstVisited,
    };
    if (auth.isLoggedin()) {
      final batch = rtdb.batch();
      // var document = rtdb
      //     .collection('users')
      //     .document(user.id)
      //     .collection('currentPlace')
      //     .document(safePlace.address);
      // batch.delete(document);
      final document = rtdb
          .collection('users')
          .document(user.id)
          .collection('currentPlace')
          .document(newData.address);
      batch.updateData(document, data);
      await batch.commit();
      final box = await Hive.openBox<CurrentPlace>('currentPlace');
      await box.delete(safePlace.address.codeUnitAt(0).toString());
      await box.put(newData.address.codeUnitAt(0).toString(), newData);
      if (box.isOpen) {
        box.close();
      }
      user.updateCurrentPlace(safePlace, newData);
      return user;
    }
    return null;
  }

  Future<User> deleteCurrentPlace(User user, CurrentPlace safePlace) async {
    if (auth.isLoggedin()) {
      await rtdb
          .collection('users')
          .document(user.id)
          .collection('currentPlace')
          .document(safePlace.address)
          .delete();
      final box = await Hive.openBox<CurrentPlace>('currentPlace');
      await box.delete(safePlace.address.codeUnitAt(0).toString());
      box.close();
      user.deleteCurrentPlace(safePlace);
      return user;
    }
    return null;
  }

  Future<void> clearCurrentPlaceLocalStorage() async {
    final box = await Hive.openBox<CurrentPlace>('currentPlace');
    await box.deleteFromDisk();
    if (box.isOpen) {
      box.close();
    }
  }
}
