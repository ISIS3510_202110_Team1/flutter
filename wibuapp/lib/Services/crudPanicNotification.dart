import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geolocator/geolocator.dart';
import 'package:connectivity/connectivity.dart';

import 'package:wibuapp/Controller/AuthController.dart';
import 'package:wibuapp/Controller/LocationController.dart';
import 'package:wibuapp/Model/user.dart';

class CrudMethodPanicNotifications {
  final Firestore rtdb = Firestore.instance;
  var auth = new AuthController();

  Future<void> addPanicNotification(
      User user, bool byAccident, String gesture) async {
    final Position currentLocation = await getCurrentLocation();
    final ConnectivityResult connectivityResult =
        await Connectivity().checkConnectivity();
    bool network = true;
    final date = DateTime.now();
    if (connectivityResult == ConnectivityResult.none) {
      network = false;
      Connectivity()
          .onConnectivityChanged
          .firstWhere((r) => r != ConnectivityResult.none)
          .then((result) async {
        if (result != ConnectivityResult.none) {
          print('connectivity is back...');
          print(result);
          final Map<String, dynamic> data = {
            'userId': user.id,
            'date': date,
            'byAccident': byAccident,
            'gesture': gesture,
            'network': network,
            'latitude': currentLocation.latitude,
            'longitude': currentLocation.longitude,
          };
          if (auth.isLoggedin()) {
            await rtdb.collection('panicNotification').add(data);
          }
        }
      });
    } else {
      final Map<String, dynamic> data = {
        'userId': user.id,
        'date': DateTime.now(),
        'byAccident': byAccident,
        'gesture': gesture,
        'network': network,
        'latitude': currentLocation.latitude,
        'longitude': currentLocation.longitude,
      };
      if (auth.isLoggedin()) {
        await rtdb.collection('panicNotification').add(data);
      }
    }
  }
}
