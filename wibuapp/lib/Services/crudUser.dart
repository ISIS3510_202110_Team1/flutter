import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hive/hive.dart';
import 'package:wibuapp/Controller/StorageController.dart';

import 'package:wibuapp/Model/user.dart';
import 'package:wibuapp/Services/crudCurrentPlace.dart';
import 'package:wibuapp/Services/crudSafePlace.dart';
import 'package:wibuapp/Services/crudSafeContact.dart';

class CrudMethodsUser {
  final Firestore rtdb = Firestore.instance;

  final CrudMethodSafePlaces safePlaceController = new CrudMethodSafePlaces();
  final CrudMethodSafeContacts safeContactController =
      new CrudMethodSafeContacts();
  final CrudMethodCurrentPlaces currentPlaceControler =
      new CrudMethodCurrentPlaces();

  Future addUser(Map<String, dynamic> userProfile, String uid) async {
    final CollectionReference users = rtdb.collection('users');
    final String rutaInterna = uid + '/' + 'profilepic/';
    final String rutaGuardar = rutaInterna + '/' + uid + '.jpg';
    final FirebStorage fireSto = new FirebStorage();
    final String downloadurl =
        await fireSto.send(userProfile['profileUrl'], rutaGuardar);
    userProfile.update('profileUrl', (value) => downloadurl);
    print(userProfile.toString());
    await users.document(uid).setData(userProfile);
  }

  Future<User> setUser(String uid) async {
    await clearUserLocalStorage();
    User user = new User();
    final DocumentSnapshot doc =
        await rtdb.collection('users').document(uid).get();
    user.id = uid;

    user.birthdate = DateTime.fromMicrosecondsSinceEpoch(
        doc.data['birthdate'].microsecondsSinceEpoch);
    user.email = doc.data['email'];
    user.firstName = doc.data['firstName'];
    user.lastName = doc.data['lastName'];
    user.gender = doc.data['gender'];
    user.telephone = doc.data['telephone'];
    user.profileUrl = doc.data['profileUrl'];
    user.lstActivity = doc.data['lstActivity'];
    user.userActivity = doc.data['userActivity'];
    if (doc.data.keys.contains('taxitimes')) {
      user.taxitimes = doc.data['taxitimes'];
    } else {
      user.taxitimes = 0;
    }

    print(user.birthdate.toString());

    final box = await Hive.openBox<User>('user');
    box.put(user.id, user);
    box.close();
    user = await safePlaceController.getSafePlaces(user);
    user = await safeContactController.getSafeContacts(user);
    user = await currentPlaceControler.getCurrentPlaces(user);

    return user;
  }

  Future updateUser(String uid, Map<String, dynamic> data) async {
    await rtdb.collection('users').document(uid).updateData(data);
  }

  Future<void> clearUserLocalStorage() async {
    final box = await Hive.openBox<User>('user');
    box.deleteFromDisk();
    box.close();
  }
}
