import 'dart:async';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:geolocator/geolocator.dart';
import 'package:connectivity/connectivity.dart';

import 'package:wibuapp/Controller/AuthController.dart';
import 'package:wibuapp/Controller/LocationController.dart';
import 'package:wibuapp/Model/user.dart';

class CrudMethodLicensePlate {
  final Firestore rtdb = Firestore.instance;
  var auth = new AuthController();

  Future<void> addLicensePlate(
      User user, String licensePlate, String from, String brand, String transportationType, bool sendToSafeContacts) async {
    final Position currentLocation = await getCurrentLocation();
    final ConnectivityResult connectivityResult =
        await Connectivity().checkConnectivity();
    bool network = true;
    final date = DateTime.now();
    if (connectivityResult == ConnectivityResult.none) {
      network = false;
      Connectivity()
          .onConnectivityChanged
          .firstWhere((r) => r != ConnectivityResult.none)
          .then((result) async {
        if (result != ConnectivityResult.none) {
          print('connectivity is back...');
          print(result);
          final Map<String, dynamic> data = {
            'userId': user.id,
            'date': date,
            'licensePlate': licensePlate,
            'from': from,
            'brand': brand,
            'transportationType': transportationType,
            'sendToSafeContacts': sendToSafeContacts,
            'network': network,
            'latitude': currentLocation.latitude,
            'longitude': currentLocation.longitude,
          };
          if (auth.isLoggedin()) {
            await rtdb.collection('licensePlates').add(data);
          }
        }
      });
    } else {
      final Map<String, dynamic> data = {
        'userId': user.id,
        'date': DateTime.now(),
        'licensePlate': licensePlate,
        'from': from,
        'brand': brand,
        'transportationType': transportationType,
        'sendToSafeContacts': sendToSafeContacts,
        'network': network,
        'latitude': currentLocation.latitude,
        'longitude': currentLocation.longitude,
      };
      if (auth.isLoggedin()) {
        await rtdb.collection('licensePlates').add(data);
      }
    }
  }
}
