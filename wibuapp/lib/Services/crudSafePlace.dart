import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:hive/hive.dart';

import 'package:wibuapp/Controller/AuthController.dart';
import 'package:wibuapp/Model/safePlace.dart';
import 'package:wibuapp/Model/user.dart';

class CrudMethodSafePlaces {
  final Firestore rtdb = Firestore.instance;
  var auth = new AuthController();

  Future<User> getSafePlaces(User user) async {
    if (auth.isLoggedin()) {
      await clearSafePlaceLocalStorage();

      final QuerySnapshot querySnapshot = await rtdb
          .collection('users')
          .document(user.id)
          .collection('places')
          .getDocuments();
      final docs = querySnapshot.documents;

      final box = await Hive.openBox<SafePlace>('safePlace');
      for (var d in docs) {
        final SafePlace safePlace = new SafePlace();
        safePlace.address = d.data['address'];
        safePlace.latitude = d.data['latitude'];
        safePlace.longitude = d.data['longitude'];
        await box.put(safePlace.address.codeUnitAt(0).toString(), safePlace);
        user.addSafePlace(safePlace);
      }
      if (box.isOpen) {
        box.close();
      }
      print('s');
      return user;
    }
    return null;
  }

  Future<User> addSafePlace(User user, SafePlace safePlace) async {
    final Map<String, dynamic> data = {
      'address': safePlace.address,
      'latitude': safePlace.latitude,
      'longitude': safePlace.longitude,
    };
    if (auth.isLoggedin()) {
      await rtdb
          .collection('users')
          .document(user.id)
          .collection('places')
          .document(safePlace.address)
          .setData(data);
      final box = await Hive.openBox<SafePlace>('safePlace');
      await box.put(safePlace.address.codeUnitAt(0).toString(), safePlace);
      box.close();
      user.addSafePlace(safePlace);
      return user;
    }
    return null;
  }

  Future<User> updateSafePlace(
      User user, SafePlace safePlace, SafePlace newData) async {
    final Map<String, dynamic> data = {
      'address': newData.address,
      'latitude': newData.latitude,
      'longitude': newData.longitude,
    };
    if (auth.isLoggedin()) {
      final batch = rtdb.batch();
      var document = rtdb
          .collection('users')
          .document(user.id)
          .collection('places')
          .document(safePlace.address);
      batch.delete(document);
      document = rtdb
          .collection('users')
          .document(user.id)
          .collection('places')
          .document(newData.address);
      batch.setData(document, data);
      await batch.commit();
      final box = await Hive.openBox<SafePlace>('safePlace');
      await box.delete(safePlace.address.codeUnitAt(0).toString());
      await box.put(newData.address.codeUnitAt(0).toString(), newData);
      box.close();
      user.updateSafePlace(safePlace, newData);
      return user;
    }
    return null;
  }

  Future deleteSafePlace(User user, SafePlace safePlace) async {
    if (auth.isLoggedin()) {
      await rtdb
          .collection('users')
          .document(user.id)
          .collection('places')
          .document(safePlace.address)
          .delete();
      final box = await Hive.openBox<SafePlace>('safePlace');
      await box.delete(safePlace.address.codeUnitAt(0).toString());
      if (box.isOpen) {
        box.close();
      }
      user.deleteSafePlace(safePlace);
      return user;
    }
    return user;
  }

  Future<List<SafePlace>> getSafePlacesLocalStorage() async {
    if (auth.isLoggedin()) {
      final box = await Hive.openBox<SafePlace>('safePlace');
      final List<SafePlace> safePlaces = box.values.toList();
      if (box.isOpen) {
        await box.close();
      }
      return safePlaces;
    }
    return null;
  }

  Future<void> clearSafePlaceLocalStorage() async {
    final box = await Hive.openBox<SafePlace>('safePlace');
    await box.deleteFromDisk();
    box.close();
  }
}
