import 'package:flutter/material.dart';
import 'package:google_maps_place_picker/google_maps_place_picker.dart';
import 'package:connectivity/connectivity.dart';
import 'package:wibuapp/Model/currentPlace.dart';
import 'package:wibuapp/Model/safePlace.dart';
import 'package:wibuapp/Model/user.dart';
import 'package:wibuapp/Services/crudCurrentPlace.dart';
import 'package:wibuapp/Services/crudSafePlace.dart';
import 'package:wibuapp/Widgets/connectionFailedMessage.dart';
import 'package:wibuapp/Widgets/offlineWarningMessage.dart';

// ignore: must_be_immutable
class CurrentPlacePage extends StatefulWidget {
  CurrentPlacePage(this.user);
  User user;
  @override
  _CurrentPlacePageState createState() => _CurrentPlacePageState(user);
}

class _CurrentPlacePageState extends State<CurrentPlacePage> {
  _CurrentPlacePageState(this._user);
  User _user;

  CrudMethodSafePlaces safePlaceController = new CrudMethodSafePlaces();
  CrudMethodCurrentPlaces ccp = new CrudMethodCurrentPlaces();

  var _connectivityResult;
  bool _warningDismissed;

  PickResult selectedPlace;

  /// Determine the current position of the device.

  void _addSafePlace(SafePlace safePlace) async {
    final User user = await safePlaceController.addSafePlace(_user, safePlace);
    setState(() {
      _user = user;
    });
  }

  void _deleteSafePlace(CurrentPlace curr) async {
    final User user = await ccp.deleteCurrentPlace(_user, curr);
    setState(() {
      _user = user;
    });
  }

  Future<void> _showMyDialog2(CurrentPlace place, Function function) async {
    final connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      connectionFailedMessage(context);
    } else if (connectivityResult == ConnectivityResult.wifi ||
        connectivityResult == ConnectivityResult.mobile) {
      print(place);
      function(place);

      final snackBar2 = SnackBar(
        content: Text('Correctly Deleted'),
        action: SnackBarAction(
          label: 'Hide',
          onPressed: () {
            // Some code to undo the change.
          },
        ),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar2);
    }
  }

  Future<void> _showMyDialog(CurrentPlace place, Function function) async {
    final connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      connectionFailedMessage(context);
    } else if (connectivityResult == ConnectivityResult.wifi ||
        connectivityResult == ConnectivityResult.mobile) {
      print(place);
      if (place.addedTosafeplace) {
        final snackBar2 = SnackBar(
          content: Text('Already Added'),
          action: SnackBarAction(
            label: 'Hide',
            onPressed: () {
              // Some code to undo the change.
            },
          ),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar2);
      } else {
        final SafePlace sf = new SafePlace();
        sf.address = place.address;
        sf.latitude = place.latitude;
        sf.longitude = place.longitude;
        print(sf.address);
        function(sf);
        final CurrentPlace newData = new CurrentPlace();
        newData.address = place.address;
        newData.latitude = place.latitude;
        newData.longitude = place.longitude;
        newData.timesvisited = place.timesvisited;
        newData.lstVisited = place.lstVisited;
        newData.addedTosafeplace = true;
        ccp.updateCurrentPlace(_user, place, newData);

        final snackBar = SnackBar(
          content: Text('Added Correctly'),
          action: SnackBarAction(
            label: 'Hide',
            onPressed: () {
              // Some code to undo the change.
            },
          ),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    }
  }

  void _dismissWarning() {
    setState(() {
      _warningDismissed = true;
    });
  }

  Future<void> _checkConnectivity() async {
    final connectivityResult = await Connectivity().checkConnectivity();
    setState(() {
      _connectivityResult = connectivityResult;
    });
    if (_connectivityResult == ConnectivityResult.none) {
      setState(() {
        _warningDismissed = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _checkConnectivity();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Recent Places'),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context),
          )),
      body: Center(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _connectivityResult == ConnectivityResult.none && !_warningDismissed
              ? offlineWarningMessage(_dismissWarning)
              : Container(),
          Expanded(
            child: ListView.separated(
              padding: const EdgeInsets.all(8),
              itemCount: _user.currentPlaces.length,
              itemBuilder: (BuildContext context, int index) {
                return ListTile(
                  leading: CircleAvatar(
                    backgroundColor: Colors.white,
                    child: Icon(Icons.place_rounded, color: Color(0xfff06400)),
                  ),
                  title: Text('${_user.currentPlaces[index].address}'),
                  subtitle: Text('Visited: ${_user.currentPlaces[index].timesvisited.toInt()} times'),
                  trailing:
                      Row(mainAxisSize: MainAxisSize.min, children: <Widget>[
                    IconButton(
                      icon: const Icon(Icons.add),
                      tooltip: 'Add as Safe Place',
                      onPressed: () => _showMyDialog(
                          _user.currentPlaces[index], _addSafePlace),
                    ),
                    IconButton(
                      icon: const Icon(Icons.delete),
                      tooltip: 'Delete',
                      onPressed: () => _showMyDialog2(
                          _user.currentPlaces[index], _deleteSafePlace),
                    ),
                  ]),
                );
              },
              separatorBuilder: (BuildContext context, int index) =>
                  const Divider(),
            ),
          ),
        ],
      )),
    );
  }
}
