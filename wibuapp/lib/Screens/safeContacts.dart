import 'package:flutter/material.dart';
import 'package:connectivity/connectivity.dart';
import 'package:contacts_service/contacts_service.dart';

import 'package:wibuapp/Model/safeContact.dart';
import 'package:wibuapp/Model/user.dart';
import 'package:wibuapp/Services/crudSafeContact.dart';
import 'package:wibuapp/Screens/addSafeContact.dart';
import 'package:wibuapp/Widgets/bottomBar.dart';
import 'package:wibuapp/Widgets/connectionFailedMessage.dart';
import 'package:wibuapp/Widgets/offlineWarningMessage.dart';
import 'package:wibuapp/Widgets/panicButton.dart';

// ignore: must_be_immutable
class SafeContactPage extends StatefulWidget {
  SafeContactPage(this.user);
  User user;
  @override
  _SafeContactPageState createState() => _SafeContactPageState(user);
}

class _SafeContactPageState extends State<SafeContactPage> {
  _SafeContactPageState(this._user);
  User _user;
  SafeContact newContact = new SafeContact();

  CrudMethodSafeContacts safeContactController = new CrudMethodSafeContacts();

  var _connectivityResult;
  bool _warningDismissed;

  void _dismissWarning() {
    setState(() {
      _warningDismissed = true;
    });
  }

  Future<void> _checkConnectivity() async {
    final connectivityResult = await Connectivity().checkConnectivity();
    setState(() {
      _connectivityResult = connectivityResult;
    });
    if (_connectivityResult == ConnectivityResult.none) {
      setState(() {
        _warningDismissed = false;
      });
    }
  }

  SafeContact parseContact(Contact contact, active) {
    final SafeContact safeContact = new SafeContact();
    if (contact != null) {
      safeContact.name = contact.displayName;
      safeContact.phone = contact.phones.first.value
          .replaceAll(' ', '')
          .replaceAll(RegExp(r'\+\d\d'), '');
      safeContact.avatar = null;
      safeContact.active = active;
    }
    return safeContact;
  }

  void addSafeContact(SafeContact safeContact, SafeContact newData) async {
    if (safeContact == null) {
      _user = await safeContactController.addSafeContact(_user, newData);
    } else {
      _user = await safeContactController.updateSafeContact(
          _user, safeContact, newData);
    }
    setState(() {});
  }

  void deleteSafeContact(SafeContact safeContact) async {
    _user = await safeContactController.deleteSafeContact(_user, safeContact);
    setState(() {});
  }

  void updateSafeContactState(SafeContact safeContact) async {
    final SafeContact newData = safeContact;
    newData.active = !safeContact.active;
    _user = await safeContactController.updateSafeContact(
        _user, safeContact, newData);
    setState(() {});
  }

  void _contactPicker(SafeContact safeContact) {
    Navigator.push(
            context,
            MaterialPageRoute(
                builder: (context) => AddSafeContactPage(_user, safeContact)))
        .then((contact) {
      var active = true;
      if (safeContact != null) {
        active = safeContact.active;
      }
      final SafeContact newData = parseContact(contact, active);
      addSafeContact(safeContact, newData);
    });
  }

  Future<void> _showConnectionFailedDialog(
      SafeContact safeContact, Function function) async {
    final connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      connectionFailedMessage(context);
    } else if (connectivityResult == ConnectivityResult.wifi ||
        connectivityResult == ConnectivityResult.mobile) {
      function(safeContact);
    }
  }

  @override
  void initState() {
    super.initState();
    _checkConnectivity();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Safe Contacts'),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context),
          )),
      body: Stack(
        children: [
          Center(
              child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisSize: MainAxisSize.min,
                  children: <Widget>[
                _connectivityResult == ConnectivityResult.none &&
                        !_warningDismissed
                    ? offlineWarningMessage(_dismissWarning)
                    : Container(),
                Expanded(
                    child: ListView.separated(
                  padding: const EdgeInsets.all(8),
                  itemCount: _user.safeContacts.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      leading: (_user.safeContacts[index].avatar != null &&
                              _user.safeContacts[index].avatar.isNotEmpty)
                          ? CircleAvatar(
                              backgroundImage:
                                  MemoryImage(_user.safeContacts[index].avatar))
                          : CircleAvatar(
                              backgroundColor: _user.safeContacts[index].active
                                  ? Color(0xfff06400)
                                  : Colors.grey,
                              child: Text(
                                _user.safeContacts[index].name
                                    .trim()
                                    .split(' ')
                                    .map((l) => l[0])
                                    .take(2)
                                    .join(),
                                style: TextStyle(color: Colors.white),
                              )),
                      title: Text('${_user.safeContacts[index].name}'),
                      subtitle: Text('${_user.safeContacts[index].phone}'),
                      trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Switch(
                              value: _user.safeContacts[index].active,
                              onChanged: (value) {
                                _showConnectionFailedDialog(
                                    _user.safeContacts[index],
                                    updateSafeContactState);
                              },
                              activeTrackColor: Color(0xff0fF064),
                              activeColor: Colors.white,
                            ),
                            PopupMenuButton(
                              child: Icon(Icons.more_vert),
                              itemBuilder: (context) => [
                                PopupMenuItem(
                                  value: 0,
                                  child: ListTile(
                                    leading: Icon(Icons.edit),
                                    title: Text('Edit'),
                                  ),
                                ),
                                PopupMenuItem(
                                  value: 1,
                                  child: ListTile(
                                    leading: Icon(Icons.delete),
                                    title: Text('Delete'),
                                  ),
                                ),
                              ],
                              onSelected: (value) {
                                if (value == 0) {
                                  _showConnectionFailedDialog(
                                      _user.safeContacts[index],
                                      _contactPicker);
                                } else if (value == 1) {
                                  _showConnectionFailedDialog(
                                      _user.safeContacts[index],
                                      deleteSafeContact);
                                }
                              },
                            ),
                          ]),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(),
                ))
              ])),
          Positioned(
            bottom: 20,
            right: 30,
            child: FloatingActionButton(
              heroTag: 'add',
              onPressed: () =>
                  {_showConnectionFailedDialog(null, _contactPicker)},
              tooltip: 'New Safe Contact',
              backgroundColor: Color(0xff0fF064),
              child: Icon(Icons.add, color: Colors.black),
            ),
          ),
        ],
      ),
      bottomNavigationBar: BottomBar(user: _user),
      floatingActionButton: PanicButton(user: _user),
      resizeToAvoidBottomInset: false,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
