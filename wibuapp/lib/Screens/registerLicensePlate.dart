import 'package:flutter/material.dart';
import 'package:wibuapp/Model/user.dart';
import 'package:wibuapp/Services/crudLicensePlate.dart';
import 'package:wibuapp/Services/crudUser.dart';
import 'package:wibuapp/Widgets/ButtonL.dart';
import 'package:wibuapp/Widgets/errorMessage.dart';
import 'package:wibuapp/Widgets/bottomBar.dart';
import 'package:wibuapp/Widgets/panicButton.dart';
import 'package:wibuapp/Controller/NotificationController.dart';

// ignore: must_be_immutable
class RegisterLicensePlatePage extends StatefulWidget {
  RegisterLicensePlatePage(this.user);
  User user;
  @override
  _RegisterLicensePlatePageState createState() =>
      _RegisterLicensePlatePageState(user);
}

class _RegisterLicensePlatePageState extends State<RegisterLicensePlatePage> {
  _RegisterLicensePlatePageState(this._user);
  final User _user;

  final TextEditingController _controller0 = TextEditingController();
  final TextEditingController _controller1 = TextEditingController();
  final TextEditingController _controller2 = TextEditingController();
  final TextEditingController _controller3 = TextEditingController();
  final TextEditingController _controller4 = TextEditingController();
  final TextEditingController _controller5 = TextEditingController();
  final _formKey = GlobalKey<FormState>();

  FocusNode _licensePlateScope;

  String _licensePlate = '';
  String _from;
  String _brand;
  String _typeOfTransportation;
  bool _sendToSafeContacts = true;

  final CrudMethodLicensePlate licensePlateController =
      new CrudMethodLicensePlate();

  final CrudMethodsUser userController = new CrudMethodsUser();

  @override
  void initState() {
    super.initState();
    _licensePlateScope = FocusNode();
  }

  void submit() async {
    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      final String error = _validateLicensePlate();
      if (error == null) {
        licensePlateController.addLicensePlate(_user, _licensePlate, _from,
            _brand, _typeOfTransportation, _sendToSafeContacts);
        if (_typeOfTransportation.toLowerCase().contains('taxi')) {
          _user.addUserTaxi();
          userController.updateUser(_user.id, _user.usermap());
        }
        if (_sendToSafeContacts) {
          sendLicensePlate(
              _user, _licensePlate, _from, _brand, _typeOfTransportation);
        }
        Navigator.pop(context);
      } else {
        showDialog(
          context: context,
          builder: (context) => errorMessage(context, error),
        );
      }
    }
  }

  void handleOnChanged(String value) {
    if (value.length == 1) {
      _licensePlate += value;
      FocusScope.of(context).nextFocus();
    }
    if (value.isEmpty) {
      FocusScope.of(context).previousFocus();
    }
  }

  void handleOnTap() {
    _controller0.text = '';
    _controller1.text = '';
    _controller2.text = '';
    _controller3.text = '';
    _controller4.text = '';
    _controller5.text = '';
    _licensePlate = '';
    _licensePlateScope.requestFocus();
    setState(() {});
  }

  String _validateLicensePlate() {
    const String patttern = r'(^[A-Z]{3}[0-9]{3}$)';
    final RegExp regExp = new RegExp(patttern);
    if (!regExp.hasMatch(_licensePlate)) {
      return 'Please enter a valid license plate';
    }
    return null;
  }

  String _validateTransportationType(String value) {
    if (value.isEmpty) {
      return 'Type of transportation is required.';
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Register License Plate'),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context),
          )),
      body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Padding(
              padding:
                  const EdgeInsets.symmetric(vertical: 50.0, horizontal: 30.0),
              child: Center(
                child: Container(
                  decoration: BoxDecoration(
                      color: Color(0xffd7d7d7),
                      borderRadius: BorderRadius.all(Radius.circular(10.0))),
                  child: Padding(
                      padding: const EdgeInsets.all(30.0),
                      child: Center(
                        child: Form(
                            key: _formKey,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Flexible(
                                          child: Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: TextFormField(
                                          controller: _controller0,
                                          keyboardType: TextInputType.text,
                                          textAlign: TextAlign.center,
                                          decoration: InputDecoration(
                                            counterText: '',
                                          ),
                                          maxLength: 1,
                                          textCapitalization:
                                              TextCapitalization.sentences,
                                          onChanged: (value) {
                                            if (value.length == 1) {
                                              _licensePlate += value;
                                              FocusScope.of(context)
                                                  .nextFocus();
                                            }
                                          },
                                          focusNode: _licensePlateScope,
                                          onTap: () => handleOnTap(),
                                        ),
                                      )),
                                      Flexible(
                                          child: Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: TextFormField(
                                            controller: _controller1,
                                            keyboardType: TextInputType.text,
                                            textAlign: TextAlign.center,
                                            decoration: InputDecoration(
                                              counterText: '',
                                            ),
                                            maxLength: 1,
                                            textCapitalization:
                                                TextCapitalization.sentences,
                                            onChanged: (value) =>
                                                handleOnChanged(value),
                                            enableInteractiveSelection: false,
                                            onTap: () => handleOnTap()),
                                      )),
                                      Flexible(
                                          child: Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: TextFormField(
                                            controller: _controller2,
                                            keyboardType: TextInputType.text,
                                            textAlign: TextAlign.center,
                                            decoration: InputDecoration(
                                              counterText: '',
                                            ),
                                            maxLength: 1,
                                            textCapitalization:
                                                TextCapitalization.sentences,
                                            onChanged: (value) =>
                                                handleOnChanged(value),
                                            enableInteractiveSelection: false,
                                            onTap: () => handleOnTap()),
                                      )),
                                      Flexible(
                                          child: Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: TextFormField(
                                            controller: _controller3,
                                            keyboardType: TextInputType.text,
                                            textAlign: TextAlign.center,
                                            decoration: InputDecoration(
                                              counterText: '',
                                            ),
                                            maxLength: 1,
                                            textCapitalization:
                                                TextCapitalization.sentences,
                                            onChanged: (value) =>
                                                handleOnChanged(value),
                                            enableInteractiveSelection: false,
                                            onTap: () => handleOnTap()),
                                      )),
                                      Flexible(
                                          child: Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: TextFormField(
                                            controller: _controller4,
                                            keyboardType: TextInputType.text,
                                            textAlign: TextAlign.center,
                                            decoration: InputDecoration(
                                              counterText: '',
                                            ),
                                            maxLength: 1,
                                            textCapitalization:
                                                TextCapitalization.sentences,
                                            onChanged: (value) =>
                                                handleOnChanged(value),
                                            enableInteractiveSelection: false,
                                            onTap: () => handleOnTap()),
                                      )),
                                      Flexible(
                                          child: Padding(
                                        padding: const EdgeInsets.all(5.0),
                                        child: TextFormField(
                                            controller: _controller5,
                                            keyboardType: TextInputType.text,
                                            textAlign: TextAlign.center,
                                            decoration: InputDecoration(
                                              counterText: '',
                                            ),
                                            maxLength: 1,
                                            textCapitalization:
                                                TextCapitalization.sentences,
                                            onChanged: (value) =>
                                                handleOnChanged(value),
                                            enableInteractiveSelection: false,
                                            onTap: () => handleOnTap()),
                                      )),
                                    ]),
                                Padding(padding: EdgeInsets.only(top: 10.0)),
                                Container(
                                    width: MediaQuery.of(context).size.width *
                                        (0.90),
                                    height: MediaQuery.of(context).size.height *
                                        (0.09),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                    ),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: Theme(
                                          data: Theme.of(context).copyWith(
                                              primaryColor: Colors.grey),
                                          child: TextFormField(
                                            style: TextStyle(),
                                            obscureText: false,
                                            decoration: new InputDecoration(
                                              isDense: true,
                                              hintText: '',
                                              labelText: 'From',
                                              hintStyle: TextStyle(),
                                            ),
                                            onChanged: (String value) {
                                              _from = value;
                                              setState(() {});
                                            },
                                          )),
                                    )),
                                Padding(padding: EdgeInsets.only(top: 10.0)),
                                Container(
                                    width: MediaQuery.of(context).size.width *
                                        (0.90),
                                    height: MediaQuery.of(context).size.height *
                                        (0.09),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                    ),
                                    child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: TextFormField(
                                          style: TextStyle(),
                                          obscureText: false,
                                          decoration: new InputDecoration(
                                            isDense: true,
                                            hintText: '',
                                            labelText: 'Brand',
                                            hintStyle: TextStyle(),
                                          ),
                                          onChanged: (String value) {
                                            _brand = value;
                                            setState(() {});
                                          },
                                        ))),
                                Padding(padding: EdgeInsets.only(top: 10.0)),
                                Container(
                                    width: MediaQuery.of(context).size.width *
                                        (0.90),
                                    height: MediaQuery.of(context).size.height *
                                        (0.09),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(10),
                                      color: Colors.white,
                                    ),
                                    child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: TextFormField(
                                          style: TextStyle(),
                                          obscureText: false,
                                          validator:
                                              _validateTransportationType,
                                          decoration: new InputDecoration(
                                            isDense: true,
                                            hintText: '',
                                            labelText: 'Type of transport',
                                            hintStyle: TextStyle(),
                                          ),
                                          onChanged: (String value) {
                                            _typeOfTransportation = value;
                                            setState(() {});
                                          },
                                        ))),
                                Padding(padding: EdgeInsets.only(top: 10.0)),
                                Row(
                                  children: <Widget>[
                                    Switch(
                                      value: _sendToSafeContacts,
                                      onChanged: (value) {
                                        _sendToSafeContacts =
                                            !_sendToSafeContacts;
                                        setState(() {});
                                      },
                                      activeTrackColor: Color(0xff0fF064),
                                      activeColor: Colors.white,
                                    ),
                                    Text('Send to safe contacts',
                                        textAlign: TextAlign.center),
                                  ],
                                ),
                                Padding(padding: EdgeInsets.only(top: 30.0)),
                                ButtonL(
                                    onPressed: () {
                                      submit();
                                    },
                                    color: Color(0xff6772fb),
                                    child: new Text(
                                      'SAVE LICENSE PLATE',
                                      style: TextStyle(
                                          color: Color(0xfffcfcff),
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width /
                                              20,
                                          fontFamily: 'Poppins'),
                                    ),
                                    minWidth:
                                        MediaQuery.of(context).size.width *
                                            (0.90),
                                    height: MediaQuery.of(context).size.height *
                                        (0.06),
                                    splashColor: Color(0xfffcfcff)),
                              ],
                            )),
                      )),
                ),
              ),
            )
          ]),
      bottomNavigationBar: BottomBar(user: _user),
      floatingActionButton: PanicButton(user: _user),
      resizeToAvoidBottomInset: false,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
