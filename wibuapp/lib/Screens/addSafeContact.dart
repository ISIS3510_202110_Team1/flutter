import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:connectivity/connectivity.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:permission_handler/permission_handler.dart';

import 'package:wibuapp/Model/user.dart';
import 'package:wibuapp/Widgets/ButtonL.dart';
import 'package:wibuapp/Widgets/offlineWarningMessage.dart';
import 'package:wibuapp/Widgets/bottomBar.dart';
import 'package:wibuapp/Widgets/panicButton.dart';
import 'package:wibuapp/Model/safeContact.dart';
import 'package:wibuapp/Screens/selectContactsDialog.dart';


// ignore: must_be_immutable
class AddSafeContactPage extends StatefulWidget {
  AddSafeContactPage(this.user, this.contact);
  User user;
  SafeContact contact;
  @override
  _AddSafeContactPageState createState() => _AddSafeContactPageState(user, contact);
}

class _AddSafeContactPageState extends State<AddSafeContactPage> {
  _AddSafeContactPageState(this._user, this.contact);
  final User _user;
  SafeContact contact;

  //Contacts
  Iterable<Contact> _contacts;
  Contact _actualContact;

  TextEditingController phoneTextFieldController = TextEditingController();
  TextEditingController guestnameTextFieldController = TextEditingController();

  final textFieldFocusNode = FocusNode();

  var _connectivityResult;
  bool _warningDismissed;

  void _dismissWarning() {
    setState(() {
      _warningDismissed = true;
    });
  }

  
  Future<void> _checkConnectivity() async {
    final connectivityResult = await Connectivity().checkConnectivity();
    setState(() {
      _connectivityResult = connectivityResult;
    });
    if (_connectivityResult == ConnectivityResult.none) {
      setState(() {
        _warningDismissed = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _checkConnectivity();
    _actualContact = new Contact();
    _actualContact.phones = [new Item()];
    if (contact != null) {
      _actualContact.displayName = contact.name;
      _actualContact.phones.first.value = contact.phone;
      _actualContact.avatar = contact.avatar;
      guestnameTextFieldController.text = _actualContact.displayName;
      phoneTextFieldController.text = _actualContact.phones.first.value;
    }
  }

  String _validateName(String value) {
    if (value.isEmpty) {
      return 'Contact name is required.';
    }
    return null;
  }

  String _validatePhone(String value) {
    const String patttern = r'(^(?:[+0]9)?[0-9]{10,12}$)';
    final RegExp regExp = new RegExp(patttern);
    if (value.isEmpty) {
      return 'Phone number is required';
    } else if (!regExp.hasMatch(value)) {
      return 'Please enter valid phone number';
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('New Safe Contact'),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context),
          )),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _connectivityResult == ConnectivityResult.none && !_warningDismissed ? offlineWarningMessage(_dismissWarning) : Container(),
          Padding(  
            padding: const EdgeInsets.symmetric(vertical: 50.0, horizontal: 30.0),
            child: Center(
              child: Container(
                decoration: BoxDecoration(
                    color: Color(0xffd7d7d7),
                    borderRadius: BorderRadius.all(Radius.circular(10.0))),
                child: Padding(
                padding: const EdgeInsets.all(30.0),
                child: Center(
                    child: Form(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                        Container(
                            width: MediaQuery.of(context).size.width * (0.90),
                            height: MediaQuery.of(context).size.height * (0.09),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.white,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Theme(
                                  data: Theme.of(context)
                                      .copyWith(primaryColor: Colors.grey),
                                  child: TextFormField(
                                    style: TextStyle(),
                                    controller: guestnameTextFieldController,
                                    obscureText: false,
                                    validator: _validateName,
                                    decoration: new InputDecoration(
                                      isDense: true,
                                      hintText: '',
                                      prefixIcon: Icon(
                                        Icons.person_outline,
                                        color: Color(0xff6360FF),
                                      ),
                                      labelText: 'Contact',
                                      hintStyle: TextStyle(),
                                      suffixIcon: GestureDetector(
                                        onTap: () {
                                          // Unfocus all focus nodes
                                          textFieldFocusNode.unfocus();

                                          // Disable text field's focus node request
                                          textFieldFocusNode.canRequestFocus =
                                              false;

                                          // Do your stuff
                                          _showContactList(context);

                                          //Enable the text field's focus node request after some delay
                                          Future.delayed(
                                              Duration(milliseconds: 100), () {
                                            textFieldFocusNode.canRequestFocus =
                                                true;
                                          });
                                        },
                                        child: Container(
                                          width: 50,
                                          height: 50,
                                          // color: Colors.red,
                                          child: Icon(
                                              Icons.perm_contact_cal_outlined,
                                              color: Color(0xff6360FF),
                                              size: 30),
                                        ),
                                      ),
                                    ),
                                    onChanged: (String value) {
                                      _actualContact.displayName = value;
                                      setState(() {});
                                    },
                                  )),
                            )),
                        Padding(padding: EdgeInsets.only(top: 10.0)),
                        Container(
                            width: MediaQuery.of(context).size.width * (0.90),
                            height: MediaQuery.of(context).size.height * (0.09),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              color: Colors.white,
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: TextFormField(
                                    style: TextStyle(),
                                    controller: phoneTextFieldController,
                                    obscureText: false,
                                    validator: _validatePhone,
                                    decoration: new InputDecoration(
                                      isDense: true,
                                      hintText: '',
                                      prefixIcon: Icon(
                                        Icons.phone_android_outlined,
                                        color: Color(0xff6360FF),
                                      ),
                                      labelText: 'Phone',
                                      hintStyle: TextStyle(),
                                    ),
                                    onChanged: (String value) {
                                      _actualContact.phones.first.value = value;
                                      setState(() {});
                                    },
                              )
                            )),
                        Padding(padding: EdgeInsets.only(top: 30.0)),
                        ButtonL(
                            onPressed: () {
                              Navigator.pop(context, _actualContact);
                            },
                            color: Color(0xff6772fb),
                            child: new Text(
                              'SAVE CONTACT',
                              style: TextStyle(
                                  color: Color(0xfffcfcff),
                                  fontSize:
                                      MediaQuery.of(context).size.width / 20,
                                  fontFamily: 'Poppins'),
                            ),
                            minWidth:
                                MediaQuery.of(context).size.width * (0.90),
                            height: MediaQuery.of(context).size.height * (0.06),
                            splashColor: Color(0xfffcfcff)),
                      ],
                    )),
                  ),
                ),
              ),
            ),
          )
        ]
      ),
      bottomNavigationBar: BottomBar(user: _user),
      floatingActionButton: PanicButton(user: _user),
      resizeToAvoidBottomInset: false,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  // Getting list of contacts from AGENDA
  void refreshContacts() async {
    final PermissionStatus permissionStatus = await _getContactPermission();
    if (permissionStatus == PermissionStatus.granted) {
      final contacts = await ContactsService.getContacts();
      setState(() {
        _contacts = contacts.where((e) => e.phones.isNotEmpty).toList();
      });
    } else {
      _handleInvalidPermissions(permissionStatus);
    }
  }

  // Asking Contact permissions
  Future<PermissionStatus> _getContactPermission() async {
    final PermissionStatus permission = await Permission.contacts.request();
    return permission;
  }

  // Managing error when you don't have permissions
  void _handleInvalidPermissions(PermissionStatus permissionStatus) {
    if (permissionStatus == PermissionStatus.denied) {
      throw new PlatformException(
          code: 'PERMISSION_DENIED',
          message: 'Access to contacts data denied',
          details: null);
    }
  }

  // Showing contact list.
  Future<Null> _showContactList(BuildContext context) async {
    const InputDecoration searchDecoration = const InputDecoration();

    refreshContacts();
    if (_contacts != null) {
      showDialog(
        context: context,
        builder: (_) => SelectionDialogContacts(
          _contacts.toList(),
          emptySearchBuilder: null,
          searchDecoration: searchDecoration,
        ),
      ).then((e) {
        if (e != null) {
          setState(() {
            _actualContact = e;
          });

          guestnameTextFieldController.text = _actualContact.displayName;
          phoneTextFieldController.text = _actualContact.phones.first.value;
        }
      });
    }
  }
}
