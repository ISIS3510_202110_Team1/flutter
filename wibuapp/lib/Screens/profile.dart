import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:image_picker/image_picker.dart';
import 'package:wibuapp/Controller/AuthController.dart';
import 'package:wibuapp/Controller/StorageController.dart';
import 'package:wibuapp/Model/user.dart';
import 'package:wibuapp/Screens/SafeContacts.dart';
import 'package:wibuapp/Services/crudUser.dart';
import 'package:wibuapp/Widgets/bottomBar.dart';
import 'package:wibuapp/Widgets/panicButton.dart';

// ignore: must_be_immutable
class ProfilePage extends StatefulWidget {
  ProfilePage(this.user, {Key key, this.child}) : super(key: key);
  User user;
  final Widget child;
  @override
  _ProfilePageState createState() => new _ProfilePageState(user);
}

class _ProfilePageState extends State<ProfilePage> {
  _ProfilePageState(this._user);
  final User _user;
  String _imageURL = '';

  File _image;

  CrudMethodsUser userController = new CrudMethodsUser();

  void _imgFromCamera() async {
    final File image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50);

    setState(() {
      _image = image;
      _imageURL = _image.path;
      updateImag();
    });
  }

  void _imgFromGallery() async {
    final File image = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50);

    setState(() {
      _image = image;
      _imageURL = _image.path;
      updateImag();
    });
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future<void> updateImag() async {
    final String rutaInterna = _user.id + '/' + 'profilepic/';
    final String rutaGuardar = rutaInterna + '/' + _user.id + '.jpg';
    final FirebStorage fireSto = new FirebStorage();
    final String downloadurl = await fireSto.send(_imageURL, rutaGuardar);
    _user.profileUrl = downloadurl;
    final ConnectivityResult connectivityResult =
        await Connectivity().checkConnectivity();
    print('yei');
    if (!(connectivityResult == ConnectivityResult.none)) {
      final Map<String, dynamic> data = {
        'id': _user.id,
        'email': _user.email,
        'firstName': _user.firstName,
        'lastName': _user.lastName,
        'birthdate': _user.birthdate,
        'gender': _user.gender,
        'telephone': _user.telephone,
        'profileUrl': _user.profileUrl
      };
      await userController.updateUser(_user.id, data);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          child: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: Color(
                0xffd7d7d7,
              ),
            ),
          ),
          Positioned(
              top: 0,
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 2,
                  decoration: BoxDecoration(
                    color: Color(
                      0xff6772fb,
                    ),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(
                        0,
                      ),
                      topRight: Radius.circular(
                        0,
                      ),
                      bottomLeft: Radius.circular(
                        44,
                      ),
                      bottomRight: Radius.circular(
                        44,
                      ),
                    ),
                  ))),
          Positioned(
              top: MediaQuery.of(context).size.height * 0.05,
              left: MediaQuery.of(context).size.width * 0.12,
              child: Text(
                'Profile',
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1),
                    fontFamily: 'DM Sans',
                    fontSize: 30,
                    letterSpacing:
                        0 /*percentages not used in flutter. defaulting to zero*/,
                    fontWeight: FontWeight.bold,
                    height: 1),
              )),
          Positioned(
              top: MediaQuery.of(context).size.height * 0.05,
              right: MediaQuery.of(context).size.width * 0.08,
              child: GestureDetector(
                  onTap: () async {
                    final AuthController auth = new AuthController();
                    auth.logOut();
                    Navigator.popUntil(context, ModalRoute.withName('/'));
                  },
                  child: Icon(Icons.logout,
                      size: 40,
                      color: Color(
                        0xffd7d7d7,
                      )))),
          Positioned(
              top: MediaQuery.of(context).size.height * 0.15,
              left: MediaQuery.of(context).size.width / 4,
              child: // Figma Flutter Generator Vectorcreator1Widget - RECTANGLE
                  Container(
                      alignment: Alignment.center,
                      width: MediaQuery.of(context).size.width / 2,
                      height: MediaQuery.of(context).size.height / 4,
                      //color: Color(0xffd7d7d7),
                      child: CachedNetworkImage(
                        imageBuilder: (context, imageProvider) => Container(
                          width: MediaQuery.of(context).size.width / 2,
                          height: MediaQuery.of(context).size.height / 4,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            image: DecorationImage(
                                alignment: Alignment.center,
                                image: imageProvider,
                                fit: BoxFit.cover),
                          ),
                        ),
                        placeholder: (context, url) =>
                            CircularProgressIndicator(),
                        imageUrl: _user.profileUrl != null
                            ? _user.profileUrl
                            : 'https://drive.google.com/uc?id=1UixAM7tzsROk-AOsnYMx73FIbBCHBAFQ',
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      ))),
          Positioned(
              top: MediaQuery.of(context).size.height * 0.15,
              right: MediaQuery.of(context).size.width / 4,
              child: GestureDetector(
                  onTap: () async {
                    _showPicker(context);
                    updateImag();
                  },
                  child: Icon(Icons.photo_camera,
                      size: 45,
                      color: Color(
                        0xffd7d7d7,
                      )))),
          Positioned(
              top: MediaQuery.of(context).size.height * 0.42,
              child: Container(
                  alignment: Alignment.center,
                  width: MediaQuery.of(context).size.width * 0.8,
                  child: Text(
                    _user.firstName + ' ' + _user.lastName,
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        color: Color.fromRGBO(255, 255, 255, 1),
                        fontFamily: 'DM Sans',
                        fontSize: 25,
                        letterSpacing:
                            1 /*percentages not used in flutter. defaulting to zero*/,
                        fontWeight: FontWeight.bold,
                        height: 1),
                  ))),
          Positioned(
              bottom: MediaQuery.of(context).size.height * 0.10,
              right: MediaQuery.of(context).size.width * 0.05,
              child: // Figma Flutter Generator Rectangle47Widget - RECTANGLE
                  GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => SafeContactPage(_user)));
                      },
                      child: Container(
                          width: MediaQuery.of(context).size.width * 0.4,
                          height: MediaQuery.of(context).size.height * 0.18,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(21),
                              topRight: Radius.circular(21),
                              bottomLeft: Radius.circular(21),
                              bottomRight: Radius.circular(21),
                            ),
                            boxShadow: [
                              BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.10),
                                  offset: Offset(0, 4),
                                  blurRadius: 4)
                            ],
                            color: Color.fromRGBO(255, 255, 255, 1),
                          ),
                          child: Stack(children: <Widget>[
                            Positioned(
                                top: MediaQuery.of(context).size.height * 0.09,
                                left: MediaQuery.of(context).size.width * 0.2,
                                child: Icon(Icons.local_taxi_outlined,
                                    size: 50, color: Color(0xffFD8C00))),
                            Positioned(
                              top: 40,
                              left: 35,
                              child: Text(
                                'My safe Plates ',
                                softWrap: true,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Color.fromRGBO(62, 68, 98, 1),
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    letterSpacing:
                                        0 /*percentages not used in flutter. defaulting to zero*/,
                                    fontWeight: FontWeight.bold,
                                    height: 1),
                              ),
                            ),
                          ])))),
          Positioned(
              bottom: MediaQuery.of(context).size.height * 0.10,
              left: MediaQuery.of(context).size.width * 0.05,
              child: // Figma Flutter Generator Rectangle47Widget - RECTANGLE
                  GestureDetector(
                      onTap: () {
                        print('plates');
                      },
                      child: Container(
                          width: MediaQuery.of(context).size.width * 0.4,
                          height: MediaQuery.of(context).size.height * 0.18,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(21),
                              topRight: Radius.circular(21),
                              bottomLeft: Radius.circular(21),
                              bottomRight: Radius.circular(21),
                            ),
                            boxShadow: [
                              BoxShadow(
                                  color: Color.fromRGBO(0, 0, 0, 0.10),
                                  offset: Offset(0, 4),
                                  blurRadius: 4)
                            ],
                            color: Color.fromRGBO(255, 255, 255, 1),
                          ),
                          child: Stack(children: <Widget>[
                            Positioned(
                                top: 40,
                                left: 50,
                                child: Icon(Icons.settings,
                                    size: 50, color: Color(0xffFD8C00))),
                            Positioned(
                              top: 100,
                              left: 50,
                              child: Text(
                                'Settings',
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                    color: Color(0xff3E4462),
                                    fontFamily: 'Poppins',
                                    fontSize: 16,
                                    letterSpacing:
                                        0 /*percentages not used in flutter. defaulting to zero*/,
                                    fontWeight: FontWeight.bold,
                                    height: 1),
                              ),
                            ),
                          ])))),
        ],
      )),
      bottomNavigationBar: BottomBar(user: _user),
      floatingActionButton: PanicButton(user: _user),
      resizeToAvoidBottomInset: false,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
