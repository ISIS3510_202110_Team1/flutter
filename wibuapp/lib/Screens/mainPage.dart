import 'package:flutter/material.dart';
import 'package:wibuapp/Model/user.dart';
import 'package:wibuapp/Screens/home.dart';

// ignore: must_be_immutable
class MainPage extends StatefulWidget {
  MainPage(this.user, {Key key, this.child}) : super(key: key);
  final Widget child;
  User user;
  @override
  _MainPageState createState() => new _MainPageState(user);
}

class _MainPageState extends State<MainPage> {
  _MainPageState(this._user);
  final User _user;


  @override
  Widget build(BuildContext context) {
    return new HomePage(_user);
  }
}
