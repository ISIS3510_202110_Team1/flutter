import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:wibuapp/Controller/AuthController.dart';
import 'package:wibuapp/Controller/CurrentPlacesController.dart';
import 'package:wibuapp/Controller/ShakeDetectorController.dart';
import 'package:wibuapp/Controller/SpeechToTexController.dart';
import 'package:wibuapp/Model/user.dart';
import 'package:validate/validate.dart';
import 'package:wibuapp/Controller/NotificationController.dart';
import 'package:wibuapp/Screens/SignIn.dart';
import 'package:wibuapp/Screens/mainPage.dart';
import 'package:wibuapp/Widgets/BuildTextFormField.dart';
import 'package:wibuapp/Widgets/ButtonL.dart';
import 'package:wibuapp/Widgets/connectionFailedMessage.dart';
import 'package:wibuapp/Widgets/errorMessage.dart';

class Login extends StatefulWidget {
  const Login();

  @override
  _Login createState() => _Login();
}

class _Login extends State<Login> {
  _Login();
  User _user;
  String _email;
  String _password;
  var auth = new AuthController();

  final _formKey = GlobalKey<FormState>();

  /// Validates de Email, it must be a email address
  String _validateEmail(String value) {
    try {
      Validate.isEmail(value);
    } catch (e) {
      return 'The E-mail Address must be a valid email address.';
    }
    return null;
  }

  String _validatePassword(String value) {
    if (value.length < 8) {
      return 'The Password must be at least 8 characters.';
    }
    return null;
  }

  void submit() async {
    final estado = await Connectivity().checkConnectivity();
    if (estado == ConnectivityResult.none) {
      connectionFailedMessage(context);
      return;
    }

    if (_formKey.currentState.validate()) {
      _formKey.currentState.save();
      auth.login(_email, _password).then((user) {
        _user = user;
        print(_user);
        final PushNotificationsHandler pushNotificationsHandler =
            new PushNotificationsHandler();
        pushNotificationsHandler.init(_user.telephone);
        backgroundLocation(_user);
        activity(_user);
        final ShakeDetectorController detector =
            new ShakeDetectorController(_user, context);
        detector.start();
        // ignore: unused_local_variable
        final SpeechToTextController speech =
            new SpeechToTextController(_user, context);
        Navigator.push(
            context, MaterialPageRoute(builder: (context) => MainPage(_user)));
      }).catchError((error) {
        print(error);
        showDialog(
          context: context,
          builder: (context) => errorMessage(
              context, error.message != null ? error.message : 'error'),
        );
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          child: Form(
              key: _formKey,
              child: Stack(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    decoration: BoxDecoration(
                      color: Color(
                        0xff6772fb,
                      ),
                    ),
                  ),
                  Positioned(
                    top: 0,
                    left: MediaQuery.of(context).size.width / 4,
                    child: // Figma Flutter Generator Vectorcreator1Widget - RECTANGLE
                        Container(
                            alignment: Alignment.centerLeft,
                            width: MediaQuery.of(context).size.width / 2,
                            height: MediaQuery.of(context).size.height / 4,
                            child: CachedNetworkImage(
                              placeholder: (context, url) =>
                                  CircularProgressIndicator(),
                              imageUrl:
                                  'https://drive.google.com/uc?id=1UixAM7tzsROk-AOsnYMx73FIbBCHBAFQ',
                              errorWidget: (context, url, error) =>
                                  Icon(Icons.error),
                            )),
                  ),
                  Positioned(
                      top: MediaQuery.of(context).size.width / 2,
                      left: MediaQuery.of(context).size.width / 3,
                      child: Container(
                          alignment: Alignment.centerLeft,
                          width: MediaQuery.of(context).size.width / 2.6,
                          height: MediaQuery.of(context).size.height / 20,
                          child: Text(
                            'WIBU',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Color.fromRGBO(255, 255, 255, 1),
                                fontFamily: 'Poppins',
                                fontSize: 60 *
                                    (MediaQuery.of(context).size.width /
                                        MediaQuery.of(context).size.height),
                                letterSpacing: -0.24,
                                fontWeight: FontWeight.normal,
                                height: 0.33),
                          ))),
                  Positioned(
                      top: MediaQuery.of(context).size.width / 1.5,
                      left: MediaQuery.of(context).size.width / 4,
                      child: Container(
                        alignment: Alignment.centerLeft,
                        width: MediaQuery.of(context).size.width / 1.6,
                        height: MediaQuery.of(context).size.height / 20,
                        child: // Figma Flutter Generator TakingcareofyouineverystepWidget - TEXT
                            Text(
                          'Taking care of you in every step',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                              color: Color.fromRGBO(252, 252, 255, 1),
                              fontFamily: 'DM Sans',
                              fontSize: 16,
                              letterSpacing:
                                  0 /*percentages not used in flutter. defaulting to zero*/,
                              fontWeight: FontWeight.normal,
                              height: 1),
                        ),
                      )),
                  Positioned(
                      top: MediaQuery.of(context).size.height / 2,
                      child: Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height / 2,
                          decoration: BoxDecoration(
                            color: Color(
                              0xffd7d7d7,
                            ),
                            borderRadius: BorderRadius.only(
                              topLeft: Radius.circular(
                                44,
                              ),
                              topRight: Radius.circular(
                                44,
                              ),
                              bottomLeft: Radius.circular(
                                0,
                              ),
                              bottomRight: Radius.circular(
                                0,
                              ),
                            ),
                          ),
                          child: Stack(
                            children: [
                              Positioned(
                                  bottom:
                                      MediaQuery.of(context).size.height / 2.7,
                                  left:
                                      MediaQuery.of(context).size.width * 0.05,
                                  child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          (0.90),
                                      height:
                                          MediaQuery.of(context).size.height *
                                              (0.09),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10),
                                          bottomLeft: Radius.circular(10),
                                          bottomRight: Radius.circular(10),
                                        ),
                                        color: Color.fromRGBO(252, 252, 255, 1),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: BuildTextFormField(
                                            null,
                                            _validateEmail,
                                            false,
                                            'you@mail.com',
                                            'E-mail', (String value) {
                                          _email = value;
                                        },
                                            Icon(
                                              Icons.email_outlined,
                                              color: Color(0xff6360FF),
                                            ),
                                            context),
                                      ))),
                              Positioned(
                                  bottom: MediaQuery.of(context).size.height *
                                      (0.26),
                                  left:
                                      MediaQuery.of(context).size.width * 0.05,
                                  child: Container(
                                      width: MediaQuery.of(context).size.width *
                                          (0.90),
                                      height:
                                          MediaQuery.of(context).size.height *
                                              (0.09),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10),
                                          bottomLeft: Radius.circular(10),
                                          bottomRight: Radius.circular(10),
                                        ),
                                        color: Color.fromRGBO(252, 252, 255, 1),
                                      ),
                                      child: Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: BuildTextFormField(
                                            null,
                                            _validatePassword,
                                            true,
                                            'Password',
                                            'Password', (String value) {
                                          _password = value;
                                        },
                                            Icon(
                                              Icons.lock,
                                              color: Color(0xff6360FF),
                                            ),
                                            context),
                                      ))),
                            ],
                          ))),
                  Positioned(
                      bottom: MediaQuery.of(context).size.height * (0.15),
                      left: MediaQuery.of(context).size.width * 0.05,
                      child: ButtonL(
                          onPressed: submit,
                          color: Color(0xff6772fb),
                          child: new Text(
                            'LOGIN',
                            style: TextStyle(
                                color: Color(0xfffcfcff),
                                fontSize:
                                    MediaQuery.of(context).size.width / 17,
                                fontFamily: 'Calibri Light'),
                          ),
                          minWidth: MediaQuery.of(context).size.width * (0.90),
                          height: MediaQuery.of(context).size.height * (0.08),
                          splashColor: Color(0xfffcfcff))),
                  Positioned(
                      bottom: MediaQuery.of(context).size.height * (0.10),
                      left: MediaQuery.of(context).size.width * 0.15,
                      child: TextButton(
                          onPressed: () => {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => SignIn()))
                              },
                          child: Text(
                            'Don’t have an account? Register now',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                                color: Color(0xffFD8C00),
                                fontFamily: 'Roboto',
                                fontSize: 15,
                                letterSpacing:
                                    0 /*percentages not used in flutter. defaulting to zero*/,
                                fontWeight: FontWeight.normal,
                                height: 1),
                          )))
                ],
              )),
          width: MediaQuery.of(context).size.width,
        ),
      ),
    );
  }
}
