import 'package:flutter/material.dart';
import 'package:connectivity/connectivity.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:google_maps_place_picker/google_maps_place_picker.dart';

import 'package:wibuapp/Widgets/connectionFailedMessage.dart';

// ignore: must_be_immutable
class AddSafePlacePage extends StatefulWidget {
  const AddSafePlacePage(this._initialPosition);
  final LatLng _initialPosition;
  @override
  _AddSafePlacePageState createState() => _AddSafePlacePageState(_initialPosition);
}

class _AddSafePlacePageState extends State<AddSafePlacePage> {
  _AddSafePlacePageState(this._initialPosition);
  final LatLng _initialPosition;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            title: Text('Select Safe Place Location'),
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              onPressed: () => Navigator.pop(context, null),
            )),
        body: PlacePicker(
          apiKey: 'AIzaSyDcm-pnxyi6fCkSeDdMfXNoph56hwdiCsg',
          initialPosition: _initialPosition,
          useCurrentLocation: false,
          selectInitialPosition: true,
          enableMapTypeButton: false,
          automaticallyImplyAppBarLeading: false,
          selectedPlaceWidgetBuilder:
              (context, result, state, isSearchBarFocused) {
            return isSearchBarFocused
                ? Container()
                : _selectedPlaceWidgetBuilder(
                    context, result, state);
          },
        ));
  }

  Widget _selectedPlaceWidgetBuilder(
      BuildContext context, result, state) {
    return FutureBuilder<ConnectivityResult>(
      future: Connectivity()
          .checkConnectivity(), // a previously-obtained Future<String> or null
      builder:
          (BuildContext context, AsyncSnapshot<ConnectivityResult> snapshot) {
        if ((snapshot.hasData && snapshot.data == ConnectivityResult.none) ||
            snapshot.hasError) {
          Future.delayed(Duration.zero, () => connectionFailedMessage(context))
              .then((value) => Navigator.of(context).pop());
          return Container();
        } else if (snapshot.hasData &&
            snapshot.data != ConnectivityResult.none) {
          return Align(
              alignment: Alignment.bottomCenter,
              child: SizedBox(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.18,
                  child: Card(
                    child: Padding(
                      padding: EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: state == SearchingState.Searching
                            ? <Widget>[
                                CircularProgressIndicator(),
                                Padding(padding: EdgeInsets.only(top: 20.0)),
                                Text('Loading location address...')
                              ]
                            : <Widget>[
                                ListTile(
                                  leading: Icon(Icons.place_rounded),
                                  title: Text('Select this location'),
                                  subtitle: Text(result.formattedAddress),
                                ),
                                SizedBox(
                                  width:
                                      MediaQuery.of(context).size.width * 0.9,
                                  // height: 50,
                                  child: ElevatedButton(
                                    onPressed: () {
                                      Navigator.pop(context, result);
                                    },
                                    child: Text('CONFIRM'),
                                  ),
                                ),
                              ],
                      ),
                    ),
                  )));
        } else {
          return Container();
        }
      },
    );
  }

}
