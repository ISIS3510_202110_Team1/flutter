import 'package:contacts_service/contacts_service.dart';
import 'package:flutter/material.dart';

/// selection dialog used for selection of the country code
class SelectionDialogContacts extends StatefulWidget {

  SelectionDialogContacts(this.elements, {
    Key key,
    this.emptySearchBuilder,
    InputDecoration searchDecoration = const InputDecoration(),
    this.searchStyle,
  }) :
        assert(searchDecoration != null, 'searchDecoration must not be null!'),
        searchDecoration = searchDecoration.copyWith(prefixIcon: Icon(Icons.search)),
        super(key: key);

  final List<Contact> elements;
  final InputDecoration searchDecoration;
  final TextStyle searchStyle;
  final WidgetBuilder emptySearchBuilder;
  @override
  State<StatefulWidget> createState() => _SelectionDialogState();
}

class _SelectionDialogState extends State<SelectionDialogContacts> {
  /// this is useful for filtering purpose
  List<Contact> filteredElements;

  @override
  Widget build(BuildContext context) => SimpleDialog(
    title: Column(
      children: <Widget>[
        TextField(
          style: widget.searchStyle,
          decoration: widget.searchDecoration,
          onChanged: _filterElements,
        ),
      ],
    ),
    children: [
      Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: ListView.builder(
          padding: const EdgeInsets.all(8),
          itemCount: filteredElements.length,
          itemBuilder: (BuildContext context, int index) {
            return ListTile(
              leading: (filteredElements[index].avatar != null && filteredElements[index].avatar.isNotEmpty)
                  ? CircleAvatar(backgroundImage: MemoryImage(filteredElements[index].avatar))
                  : CircleAvatar(backgroundColor: Color(0xfff06400), child: Text(filteredElements[index].initials(), style: TextStyle(color: Colors.white),)),
              title: Text('${filteredElements[index].displayName}'),
              subtitle: Text('${filteredElements[index].phones.first.value}'),
              onTap: () => _selectItem(filteredElements[index]),
            );
          },
        )
      ),
    ],
  );

  // Widget _buildEmptySearchWidget(BuildContext context) {
  //   if (widget.emptySearchBuilder != null) {
  //     return widget.emptySearchBuilder(context);
  //   }

  //   return Center(child: Text('No hay contactos en su agenda'));
  // }

  @override
  void initState() {
    filteredElements = widget.elements;
    super.initState();
  }

  void _filterElements(String s) {
    s = s.toLowerCase();
    setState(() {
      filteredElements = widget.elements
          .where((e) =>
          e.displayName.toLowerCase().contains(s))
          .toList();
    });
  }

  void _selectItem(Contact e) {
    Navigator.pop(context, e);
  }
}