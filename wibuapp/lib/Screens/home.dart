import 'dart:async';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';
import 'package:wibuapp/Model/currentPlace.dart';
import 'package:wibuapp/Model/user.dart';
import 'package:wibuapp/Screens/registerLicensePlate.dart';
import 'package:wibuapp/Screens/SafePlaces.dart';
import 'package:wibuapp/Screens/currentLocation.dart';
import 'package:wibuapp/Screens/safeContacts.dart';
import 'package:wibuapp/Services/crudCurrentPlace.dart';
import 'package:wibuapp/Services/crudUser.dart';
import 'package:wibuapp/Widgets/bottomBar.dart';
import 'package:wibuapp/Widgets/panicButton.dart';

// ignore: must_be_immutable
class HomePage extends StatefulWidget {
  HomePage(this.user, {Key key, this.child}) : super(key: key);
  User user;
  final Widget child;
  @override
  _HomePageState createState() => new _HomePageState(user);
}

class _HomePageState extends State<HomePage> {
  _HomePageState(this._user);
  final User _user;

  CrudMethodCurrentPlaces currentPlaceController =
      new CrudMethodCurrentPlaces();
  var _connectivityResult;
  List<CurrentPlace> cp = <CurrentPlace>[];
  CrudMethodsUser userControler = new CrudMethodsUser();
  CrudMethodCurrentPlaces ccp = new CrudMethodCurrentPlaces();

  @override
  void initState() {
    super.initState();
    // _init();
    _checkConnectivity();
  }

  Future<void> _checkConnectivity() async {
    final connectivityResult = await Connectivity().checkConnectivity();
    setState(() {
      _connectivityResult = connectivityResult;
    });
    if (_connectivityResult == ConnectivityResult.none) {
      cp = await ccp.getCurrentPlaceLocalStorage();
      setState(() {});
    } else {
      cp = _user.currentPlaces;
    }
  }

  // void onData(ActivityEvent activityEvent) async {
  //   print(activityEvent.toString());
  //   setState(() {
  //     _events.add(activityEvent);
  //     latestActivity = activityEvent;
  //   });
  //   _user.updateLstActivity(latestActivity.type.toString());
  //   _user.addUserACT(latestActivity.type.toString());
  // }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      body: Container(
          child: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            decoration: BoxDecoration(
              color: Color(
                0xffd7d7d7,
              ),
            ),
          ),
          Positioned(
              top: 0,
              child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height / 2,
                  decoration: BoxDecoration(
                    color: Color(
                      0xff6772fb,
                    ),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(
                        0,
                      ),
                      topRight: Radius.circular(
                        0,
                      ),
                      bottomLeft: Radius.circular(
                        44,
                      ),
                      bottomRight: Radius.circular(
                        44,
                      ),
                    ),
                  ))),
          Positioned(
              top: MediaQuery.of(context).size.height * 0.1,
              left: MediaQuery.of(context).size.width * 0.05,
              child: Text(
                'Hi ' + _user.firstName,
                textAlign: TextAlign.center,
                style: TextStyle(
                    color: Color.fromRGBO(255, 255, 255, 1),
                    fontFamily: 'DM Sans',
                    fontSize: 40,
                    letterSpacing:
                        0 /*percentages not used in flutter. defaulting to zero*/,
                    fontWeight: FontWeight.normal,
                    height: 1),
              )),
          Positioned(
              top: MediaQuery.of(context).size.height * 0.2,
              left: MediaQuery.of(context).size.width / 4,
              child: // Figma Flutter Generator Vectorcreator1Widget - RECTANGLE
                  Container(
                      alignment: Alignment.centerLeft,
                      width: MediaQuery.of(context).size.width / 2,
                      height: MediaQuery.of(context).size.height / 4,
                      //color: Color(0xffd7d7d7),
                      child: CachedNetworkImage(
                        placeholder: (context, url) =>
                            CircularProgressIndicator(),
                        imageUrl:
                            'https://drive.google.com/uc?id=1FHSBevIuMLsjT5IlNJCmvTwhRMBb5O09',
                        errorWidget: (context, url, error) => Icon(Icons.error),
                      )

                      // decoration: BoxDecoration(
                      //   //    color: Color(0xffd7d7d7),
                      //   image: DecorationImage(
                      //       image: AssetImage('assets/panic.png'),
                      //       fit: BoxFit.fitWidth),
                      )),
          Positioned(
              bottom: MediaQuery.of(context).size.height * 0.02,
              left: MediaQuery.of(context).size.width * 0.05,
              child: // Figma Flutter Generator Rectangle47Widget - RECTANGLE
                  Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            final snackBar2 = SnackBar(
                              content: Text('You have traveled ' +
                                  _user.taxitimes.toString() +
                                  ' time in taxi'),
                              action: SnackBarAction(
                                label: 'Hide',
                                onPressed: () {
                                  // Some code to undo the change.
                                },
                              ),
                            );
                            ScaffoldMessenger.of(context)
                                .showSnackBar(snackBar2);
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    // builder: (context) => SafeContactPage(_user)
                                    builder: (context) =>
                                        RegisterLicensePlatePage(_user)));
                          },
                          child: Container(
                              width: MediaQuery.of(context).size.width * 0.4,
                              height: MediaQuery.of(context).size.height * 0.18,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(21),
                                  topRight: Radius.circular(21),
                                  bottomLeft: Radius.circular(21),
                                  bottomRight: Radius.circular(21),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.10),
                                      offset: Offset(0, 4),
                                      blurRadius: 4)
                                ],
                                color: Color.fromRGBO(255, 255, 255, 1),
                              ),
                              child: Stack(children: <Widget>[
                                Positioned(
                                    top: 20,
                                    left: 60,
                                    child: Icon(Icons.local_taxi,
                                        size: 50, color: Color(0xffFD8C00))),
                                Positioned(
                                  top: 70,
                                  left: 30,
                                  right: 30,
                                  child: Text(
                                    'Register License Plate ',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Color.fromRGBO(62, 68, 98, 1),
                                        fontFamily: 'Poppins',
                                        fontSize: 14,
                                        letterSpacing:
                                            0 /*percentages not used in flutter. defaulting to zero*/,
                                        fontWeight: FontWeight.normal,
                                        height: 1),
                                    maxLines: 2,
                                    overflow: TextOverflow.clip,
                                  ),
                                ),
                              ])),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 50,
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    // builder: (context) => SafeContactPage(_user)
                                    builder: (context) =>
                                        SafeContactPage(_user)));
                          },
                          child: Container(
                              width: MediaQuery.of(context).size.width * 0.4,
                              height: MediaQuery.of(context).size.height * 0.18,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(21),
                                  topRight: Radius.circular(21),
                                  bottomLeft: Radius.circular(21),
                                  bottomRight: Radius.circular(21),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.10),
                                      offset: Offset(0, 4),
                                      blurRadius: 4)
                                ],
                                color: Color.fromRGBO(255, 255, 255, 1),
                              ),
                              child: Stack(children: <Widget>[
                                Positioned(
                                    top: 20,
                                    left: 60,
                                    child: Icon(Icons.shield,
                                        size: 50, color: Color(0xffFD8C00))),
                                Positioned(
                                  top: 100,
                                  left: 20,
                                  child: Text(
                                    'My safe Contacts ',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Color.fromRGBO(62, 68, 98, 1),
                                        fontFamily: 'Poppins',
                                        fontSize: 16,
                                        letterSpacing:
                                            0 /*percentages not used in flutter. defaulting to zero*/,
                                        fontWeight: FontWeight.normal,
                                        height: 1),
                                  ),
                                ),
                              ])),
                        )
                      ],
                    ),
                    SizedBox(
                      width: MediaQuery.of(context).size.width / 10,
                    ),
                    Column(
                      children: [
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    // builder: (context) => SafeContactPage(_user)
                                    builder: (context) =>
                                        SafePlacePage(_user)));
                          },
                          child: Container(
                              width: MediaQuery.of(context).size.width * 0.4,
                              height: MediaQuery.of(context).size.height * 0.18,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(21),
                                  topRight: Radius.circular(21),
                                  bottomLeft: Radius.circular(21),
                                  bottomRight: Radius.circular(21),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.10),
                                      offset: Offset(0, 4),
                                      blurRadius: 4)
                                ],
                                color: Color.fromRGBO(255, 255, 255, 1),
                              ),
                              child: Stack(children: <Widget>[
                                Positioned(
                                    top: 40,
                                    left: 60,
                                    child: Icon(Icons.location_pin,
                                        size: 50, color: Color(0xffFD8C00))),
                                Positioned(
                                  top: 100,
                                  left: 20,
                                  child: Text(
                                    'Safe Places ',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Color.fromRGBO(62, 68, 98, 1),
                                        fontFamily: 'Poppins',
                                        fontSize: 16,
                                        letterSpacing:
                                            0 /*percentages not used in flutter. defaulting to zero*/,
                                        fontWeight: FontWeight.normal,
                                        height: 1),
                                  ),
                                ),
                              ])),
                        ),
                        SizedBox(
                          height: MediaQuery.of(context).size.height / 50,
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                                context,
                                MaterialPageRoute(
                                    // builder: (context) => SafeContactPage(_user)
                                    builder: (context) =>
                                        CurrentPlacePage(_user)));
                          },
                          child: Container(
                              width: MediaQuery.of(context).size.width * 0.4,
                              height: MediaQuery.of(context).size.height * 0.18,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.only(
                                  topLeft: Radius.circular(21),
                                  topRight: Radius.circular(21),
                                  bottomLeft: Radius.circular(21),
                                  bottomRight: Radius.circular(21),
                                ),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color.fromRGBO(0, 0, 0, 0.10),
                                      offset: Offset(0, 4),
                                      blurRadius: 4)
                                ],
                                color: Color.fromRGBO(255, 255, 255, 1),
                              ),
                              child: Stack(children: <Widget>[
                                Positioned(
                                    top: 20,
                                    left: 60,
                                    child: Icon(Icons.location_searching_sharp,
                                        size: 50, color: Color(0xffFD8C00))),
                                Positioned(
                                  top: 70,
                                  left: 20,
                                  child: Text(
                                    'My Current Places',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Color.fromRGBO(62, 68, 98, 1),
                                        fontFamily: 'Poppins',
                                        fontSize: 16,
                                        letterSpacing:
                                            0 /*percentages not used in flutter. defaulting to zero*/,
                                        fontWeight: FontWeight.normal,
                                        height: 1),
                                    maxLines: 2,
                                    overflow: TextOverflow.clip,
                                  ),
                                ),
                              ])),
                        )
                      ],
                    )
                  ])),
          Positioned(
              bottom: MediaQuery.of(context).size.height * 0.148,
              left: MediaQuery.of(context).size.width * 0.50,
              child: Stack(children: [
                Positioned(
                    child: new IconButton(
                  icon: Icon(
                    Icons.notifications,
                    size: 50,
                  ),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => CurrentPlacePage(_user)));
                  },
                )),
                cp.isNotEmpty
                    ? new Positioned(
                        right: 11,
                        top: 11,
                        child: new Container(
                          padding: EdgeInsets.all(2),
                          decoration: new BoxDecoration(
                            color: Colors.red,
                            borderRadius: BorderRadius.circular(10),
                          ),
                          constraints: BoxConstraints(
                            minWidth: 14,
                            minHeight: 14,
                          ),
                          child: Text(
                            cp.length.toString(),
                            style: TextStyle(
                              color: Colors.white,
                              fontSize: 10,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      )
                    : new Container()
              ])),
        ],
      )),
      bottomNavigationBar: BottomBar(user: _user),
      floatingActionButton: PanicButton(user: _user),
      resizeToAvoidBottomInset: false,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
