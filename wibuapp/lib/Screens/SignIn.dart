import 'dart:io';
import 'dart:ui';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'package:validate/validate.dart';
import 'package:wibuapp/Controller/AuthController.dart';
import 'package:wibuapp/Services/crudUser.dart';
import 'package:wibuapp/Widgets/BuildTextFormField.dart';
import 'package:wibuapp/Widgets/ButtonL.dart';
import 'package:wibuapp/Widgets/approvedMessage.dart';
import 'package:wibuapp/Widgets/errorMessage.dart';
import 'package:wibuapp/Widgets/connectionFailedMessage.dart';
import 'package:wibuapp/Widgets/warningMessage.dart';

class SignIn extends StatefulWidget {
  @override
  _SignInState createState() => new _SignInState();
}

class _SignInState extends State<SignIn> {
  final _formKey = GlobalKey<FormState>();
  String _email;
  String _firstName;
  String _lastName;
  String _gender;
  String _telephone;
  String _password;
  DateTime _birthDay = new DateTime.now();
  String _imageURL = '';
  int _sexo;
  File _image;

  var auth = new AuthController();

  /// selecciona el dia de nacimiento
  Future seleccioarDia(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: _birthDay,
        firstDate: DateTime(1910, 1),
        lastDate: DateTime.now());
    builder:
    (BuildContext context, Widget child) {
      return Theme(
        data: Theme.of(context).copyWith(
            colorScheme: ColorScheme.light(
              primary: Colors.yellow, // header background color
              onPrimary: Colors.black, // header text color
              onSurface: Colors.green, // body text color
            ),
            textButtonTheme: TextButtonThemeData(
              style: TextButton.styleFrom(
                primary: Colors.red, // button text color
              ),
              //selection color
              //dialogBackgroundColor: Colors.white,//Background color
            )),
        child: child,
      );
    };

    if (picked != null && picked != _birthDay)
      setState(() {
        _birthDay = picked;
      });
  }

  /// Maneja el estado del RadioBotton
  void handleRadioValueChange(int value) {
    setState(() {
      _sexo = value;
      switch (value) {
        case 0:
          _gender = 'F';
          break;
        case 1:
          _gender = 'M';
          break;

        default:
          _gender = 'O';
      }
    });
  }

  void _imgFromCamera() async {
    final File image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50);

    setState(() {
      _image = image;
      _imageURL = _image.path;
    });
  }

  void _imgFromGallery() async {
    final File image = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50);

    setState(() {
      _image = image;
      _imageURL = _image.path;
    });
  }

  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  Future submit() async {
    // First validate form.
    final estado = await Connectivity().checkConnectivity();
    if (estado == ConnectivityResult.none) {
      connectionFailedMessage(context);
      return;
    }
    if (_formKey.currentState.validate() && _image != null) {
      _formKey.currentState.save(); // Save our form now.
      try {
        // creates the firebase user
        final AuthController auth = new AuthController();
        final user = await auth.createUser(_email, _password);
        //creates Map to add to firestore
        final CrudMethodsUser cd = new CrudMethodsUser();
        final Map<String, dynamic> data = {
          'email': _email,
          'firstName': _firstName,
          'lastName': _lastName,
          'telephone': _telephone,
          'gender': _gender,
          'birthdate': _birthDay,
          'profileUrl': _imageURL,
          'id': user.uid,
          'taxitimes': 0,
        };
        // adds it to the database.
        await cd.addUser(data, user.uid);
        //success
        await showDialog(
            context: context,
            builder: (context) =>
                approvedMessage(context, '!Sign In Sucesfull!')).then((value) {
          Navigator.pop(context);
        });
      } catch (e) {
        showDialog(
          context: context,
          builder: (context) => errorMessage(context, e.message),
        );
      }
    } else {
      showDialog(
        context: context,
        builder: (context) =>
            warningMessage(context, 'Please Pick a profile Pick'),
      );
    }
  }

  /// Validates de Email, it must be a email address
  String _validateEmail(String value) {
    try {
      Validate.isEmail(value);
    } catch (e) {
      return 'The E-mail Address must be a valid email address.';
    }
    return null;
  }

  String _validatePassword(String value) {
    if (value.length < 8) {
      return 'The Password must be at least 8 characters.';
    }
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Container(
                child: Form(
                    key: _formKey,
                    child: Stack(
                      children: [
                        Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          decoration: BoxDecoration(
                            color: Color(
                              0xff6772fb,
                            ),
                          ),
                        ),
                        Positioned(
                            top: MediaQuery.of(context).size.width / 6,
                            left: MediaQuery.of(context).size.width / 3,
                            child: Container(
                                alignment: Alignment.centerLeft,
                                width: MediaQuery.of(context).size.width / 2.6,
                                height: MediaQuery.of(context).size.height / 20,
                                child: Text(
                                  'Sign In',
                                  textAlign: TextAlign.center,
                                  style: TextStyle(
                                      color: Color.fromRGBO(255, 255, 255, 1),
                                      fontFamily: 'Poppins',
                                      fontSize: 45,
                                      letterSpacing: -0.24,
                                      fontWeight: FontWeight.normal,
                                      height: 0.33),
                                ))),
                        Positioned(
                            top: MediaQuery.of(context).size.height * 0.10,
                            left: MediaQuery.of(context).size.width / 4,
                            child: // Figma Flutter Generator Vectorcreator1Widget - RECTANGLE
                                Container(
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width / 2,
                              height: MediaQuery.of(context).size.height / 4,
                              //color: Color(0xffd7d7d7),
                              child: _image == null
                                  ? new CircleAvatar(
                                      backgroundImage:
                                          AssetImage('assets/wibu.png'),
                                      radius: 70.0,
                                    )
                                  : new CircleAvatar(
                                      backgroundImage: new FileImage(_image),
                                      radius: 70,
                                    ),
                            )),
                        Positioned(
                            top: MediaQuery.of(context).size.height * 0.12,
                            right: MediaQuery.of(context).size.width * (0.3),
                            child: GestureDetector(
                                onTap: () async {
                                  _showPicker(context);
                                },
                                child: Icon(Icons.photo_camera,
                                    size: 40,
                                    color: Color(
                                      0xffd7d7d7,
                                    )))),
                        Positioned(
                            top: MediaQuery.of(context).size.height * (0.30),
                            left: MediaQuery.of(context).size.width * 0.05,
                            child: Container(
                                width:
                                    MediaQuery.of(context).size.width * (0.90),
                                height:
                                    MediaQuery.of(context).size.height * (0.07),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                    bottomRight: Radius.circular(10),
                                  ),
                                  color: Color.fromRGBO(252, 252, 255, 1),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: BuildTextFormField(
                                      null, null, false, '', 'First Name',
                                      (String value) {
                                    _firstName = value;
                                  },
                                      Icon(
                                        Icons.person,
                                        color: Color(0xff6360FF),
                                      ),
                                      context),
                                ))),
                        Positioned(
                            top: MediaQuery.of(context).size.height * (0.38),
                            left: MediaQuery.of(context).size.width * 0.05,
                            child: Container(
                                width:
                                    MediaQuery.of(context).size.width * (0.90),
                                height:
                                    MediaQuery.of(context).size.height * (0.07),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                    bottomRight: Radius.circular(10),
                                  ),
                                  color: Color.fromRGBO(252, 252, 255, 1),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: BuildTextFormField(
                                      null, null, false, '', 'Lastname',
                                      (String value) {
                                    _lastName = value;
                                  },
                                      Icon(
                                        Icons.person,
                                        color: Color(0xff6360FF),
                                      ),
                                      context),
                                ))),
                        Positioned(
                            top: MediaQuery.of(context).size.height * (0.46),
                            left: MediaQuery.of(context).size.width * 0.05,
                            child: Container(
                                height:
                                    MediaQuery.of(context).size.height * (0.07),
                                width:
                                    MediaQuery.of(context).size.width * (0.90),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      topRight: Radius.circular(10),
                                      bottomLeft: Radius.circular(10),
                                      bottomRight: Radius.circular(10),
                                    )),
                                child: Row(
                                  children: <Widget>[
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width /
                                          20,
                                    ),
                                    Container(
                                      child: Icon(Icons.calendar_today,
                                          size: 20,
                                          color: Color(
                                            0xff6772fb,
                                          )),
                                    ),
                                    SizedBox(
                                      width: MediaQuery.of(context).size.width /
                                          50,
                                    ),
                                    Text('Birthdate',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.grey,
                                            fontSize: MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                40,
                                            fontWeight: FontWeight.normal,
                                            fontFamily: 'Calibri Light')),
                                    SizedBox(
                                      height:
                                          MediaQuery.of(context).size.height /
                                              80,
                                    ),
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: <Widget>[
                                        SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                25),
                                        GestureDetector(
                                            onTap: () {
                                              seleccioarDia(context);
                                            },
                                            child: Text(
                                                '${_birthDay.day}/${_birthDay.month}/${_birthDay.year}',
                                                style: TextStyle(
                                                    color: Colors.grey,
                                                    fontSize:
                                                        MediaQuery.of(context)
                                                                .size
                                                                .height /
                                                            40,
                                                    fontFamily:
                                                        'Calibri Light'))),
                                      ],
                                    )
                                  ],
                                ))),
                        Positioned(
                            top: MediaQuery.of(context).size.height * (0.54),
                            left: MediaQuery.of(context).size.width * 0.05,
                            child: Container(
                                width:
                                    MediaQuery.of(context).size.width * (0.90),
                                height:
                                    MediaQuery.of(context).size.height * (0.07),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                    bottomRight: Radius.circular(10),
                                  ),
                                  color: Color.fromRGBO(252, 252, 255, 1),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: BuildTextFormField(
                                      null,
                                      _validateEmail,
                                      false,
                                      'you@mail.com',
                                      'E-mail', (String value) {
                                    _email = value;
                                  },
                                      Icon(
                                        Icons.email_outlined,
                                        color: Color(0xff6360FF),
                                      ),
                                      context),
                                ))),
                        Positioned(
                            bottom: MediaQuery.of(context).size.height * (0.31),
                            left: MediaQuery.of(context).size.width * 0.05,
                            child: Container(
                                width:
                                    MediaQuery.of(context).size.width * (0.90),
                                height:
                                    MediaQuery.of(context).size.height * (0.07),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                    bottomRight: Radius.circular(10),
                                  ),
                                  color: Color.fromRGBO(252, 252, 255, 1),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: BuildTextFormField(
                                      null,
                                      _validatePassword,
                                      true,
                                      'Password',
                                      'Password', (String value) {
                                    _password = value;
                                  },
                                      Icon(
                                        Icons.lock,
                                        color: Color(0xff6360FF),
                                      ),
                                      context),
                                ))),
                        Positioned(
                            bottom: MediaQuery.of(context).size.height * 0.23,
                            left: MediaQuery.of(context).size.width * 0.05,
                            child: Container(
                                width:
                                    MediaQuery.of(context).size.width * (0.90),
                                height:
                                    MediaQuery.of(context).size.height * (0.07),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(10),
                                    topRight: Radius.circular(10),
                                    bottomLeft: Radius.circular(10),
                                    bottomRight: Radius.circular(10),
                                  ),
                                  color: Color.fromRGBO(252, 252, 255, 1),
                                ),
                                child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: BuildTextFormField(
                                      null,
                                      null,
                                      false,
                                      'Telephone',
                                      '(300)-777-7777', (String value) {
                                    _telephone = value;
                                  },
                                      Icon(
                                        Icons.phone,
                                        color: Color(0xff6360FF),
                                      ),
                                      context),
                                ))),
                        Positioned(
                            bottom: MediaQuery.of(context).size.height * (0.12),
                            left: MediaQuery.of(context).size.width * 0.05,
                            child: Container(
                                width:
                                    MediaQuery.of(context).size.width * (0.90),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      topRight: Radius.circular(10),
                                      bottomLeft: Radius.circular(10),
                                      bottomRight: Radius.circular(10),
                                    )),
                                child: Column(children: <Widget>[
                                  SizedBox(
                                    height: 2,
                                  ),
                                  Text('Gender',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.grey,
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              40,
                                          fontWeight: FontWeight.bold,
                                          fontFamily: 'Calibri Light')),
                                  SizedBox(
                                    height: 2,
                                  ),
                                  Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Radio(
                                        groupValue: _sexo,
                                        value: 0,
                                        onChanged: handleRadioValueChange,
                                        activeColor: Color(
                                          0xff6772fb,
                                        ),
                                      ),
                                      Text(
                                        'F',
                                        style: TextStyle(
                                            color: Color(0xff5A5353),
                                            fontSize: MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                45,
                                            fontFamily: 'Calibri Light'),
                                      ),
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Radio(
                                        groupValue: _sexo,
                                        value: 1,
                                        onChanged: handleRadioValueChange,
                                        activeColor: Color(
                                          0xff6772fb,
                                        ),
                                      ),
                                      Text(
                                        'M',
                                        style: TextStyle(
                                            color: Color(0xff5A5353),
                                            fontSize: MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                45,
                                            fontFamily: 'Calibri Light'),
                                      ),
                                      SizedBox(
                                        width: 2,
                                      ),
                                      Radio(
                                        groupValue: _sexo,
                                        value: 2,
                                        onChanged: handleRadioValueChange,
                                        activeColor: Color(
                                          0xff6772fb,
                                        ),
                                      ),
                                      Text(
                                        'O',
                                        style: TextStyle(
                                            color: Color(0xff5A5353),
                                            fontSize: MediaQuery.of(context)
                                                    .size
                                                    .height /
                                                45,
                                            fontFamily: 'Calibri Light'),
                                      )
                                    ],
                                  )
                                ]))),
                        Positioned(
                            bottom: MediaQuery.of(context).size.height * (0.04),
                            left: MediaQuery.of(context).size.width * 0.05,
                            child: ButtonL(
                                onPressed: submit,
                                color: Color(0xffFD8C00),
                                child: new Text(
                                  'SIGN IN',
                                  style: TextStyle(
                                      color: Color(0xfffcfcff),
                                      fontSize:
                                          MediaQuery.of(context).size.width /
                                              17,
                                      fontFamily: 'Calibri Light'),
                                ),
                                minWidth:
                                    MediaQuery.of(context).size.width * (0.90),
                                height:
                                    MediaQuery.of(context).size.height * (0.07),
                                splashColor: Color(0xfffcfcff))),
                      ],
                    )))));
  }
}
