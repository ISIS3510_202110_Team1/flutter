import 'dart:async';
import 'package:flutter/material.dart';
import 'package:google_maps_place_picker/google_maps_place_picker.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:geolocator/geolocator.dart';
import 'package:connectivity/connectivity.dart';
import 'package:wibuapp/Model/currentPlace.dart';
import 'package:wibuapp/Model/safePlace.dart';
import 'package:wibuapp/Model/user.dart';
import 'package:wibuapp/Services/crudSafePlace.dart';
import 'package:wibuapp/Widgets/connectionFailedMessage.dart';
import 'package:wibuapp/Widgets/offlineWarningMessage.dart';
import 'package:wibuapp/Widgets/bottomBar.dart';
import 'package:wibuapp/Screens/addSafePlace.dart';
import 'package:wibuapp/Widgets/panicButton.dart';
import 'package:firebase_analytics/firebase_analytics.dart';

// ignore: must_be_immutable
class SafePlacePage extends StatefulWidget {
  SafePlacePage(this.user);
  User user;
  @override
  _SafePlacePageState createState() => _SafePlacePageState(user);
}

class _SafePlacePageState extends State<SafePlacePage> {
  _SafePlacePageState(this._user);
  User _user;

  CrudMethodSafePlaces safePlaceController = new CrudMethodSafePlaces();

  var _connectivityResult;
  bool _warningDismissed;
  List<CurrentPlace> currentP;
  FirebaseAnalytics analytics = FirebaseAnalytics();

  PickResult selectedPlace;

  /// Determine the current position of the device.
  Future<Position> _getCurrentPosition() async {
    bool serviceEnabled;
    LocationPermission permission;

    // Test if location services are enabled.
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      // Location services are not enabled don't continue
      // accessing the position and request users of the
      // App to enable the location services.
      return Future.error('Location services are disabled.');
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission == LocationPermission.deniedForever) {
        // Permissions are denied forever, handle appropriately.
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }

      if (permission == LocationPermission.denied) {
        // Permissions are denied, next time you could try
        // requesting permissions again (this is also where
        // Android's shouldShowRequestPermissionRationale
        // returned true. According to Android guidelines
        // your App should show an explanatory UI now.
        return Future.error('Location permissions are denied');
      }
    }

    // When we reach here, permissions are granted and we can
    // continue accessing the position of the device.
    return Geolocator.getCurrentPosition();
  }

  void _deleteSafePlace(SafePlace safePlace) async {
    _user = await safePlaceController.deleteSafePlace(_user, safePlace);
    setState(() {});
  }

  Future<void> _sendAnalyticsEvent(name) async {
    await analytics.logEvent(
      name: name,
      parameters: <String, dynamic>{
        'string': _user.id,
        'int': 42,
        'long': 12345678910,
        'double': 42.0,
        'bool': true,
      },
    );
    print('sendtEvent');
  }

  void confirm(result, safePlace) async {
    if (result != null) {
      selectedPlace = result;
      final SafePlace newData = new SafePlace();
      newData.address = selectedPlace.formattedAddress;
      newData.latitude = selectedPlace.geometry.location.lat;
      newData.longitude = selectedPlace.geometry.location.lng;
      if (safePlace != null) {
        _user = await safePlaceController.updateSafePlace(
            _user, safePlace, newData);
      } else {
        _user = await safePlaceController.addSafePlace(_user, newData);
        await _sendAnalyticsEvent('SafePlaceCreate');
        print('sendt');
      }
      setState(() {});
    }
  }

  void _placePicker(SafePlace currentLocation) async {
    LatLng initialPosition;
    await _sendAnalyticsEvent('tryCreateSafePlace');
    print('sendt');
    if (currentLocation == null) {
      final Position _locationData = await _getCurrentPosition();
      initialPosition = LatLng(_locationData.latitude, _locationData.longitude);
    } else {
      initialPosition =
          LatLng(currentLocation.latitude, currentLocation.longitude);
    }

    Navigator.push(
      context,
      MaterialPageRoute(
          builder: (context) => AddSafePlacePage(initialPosition)),
    ).then((result) => confirm(result, currentLocation));
  }

  Future<void> _showMyDialog(
      SafePlace currentLocation, Function function) async {
    final connectivityResult = await Connectivity().checkConnectivity();
    if (connectivityResult == ConnectivityResult.none) {
      connectionFailedMessage(context);
    } else if (connectivityResult == ConnectivityResult.wifi ||
        connectivityResult == ConnectivityResult.mobile) {
      function(currentLocation);
    }
  }

  void _dismissWarning() {
    setState(() {
      _warningDismissed = true;
    });
  }

  Future<void> _checkConnectivity() async {
    final connectivityResult = await Connectivity().checkConnectivity();
    setState(() {
      _connectivityResult = connectivityResult;
    });
    if (_connectivityResult == ConnectivityResult.none) {
      setState(() {
        _warningDismissed = false;
      });
    }
  }

  @override
  void initState() {
    super.initState();
    _checkConnectivity();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Safe Places'),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context),
          )),
      body: Stack(children: [
        Center(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              _connectivityResult == ConnectivityResult.none &&
                      !_warningDismissed
                  ? offlineWarningMessage(_dismissWarning)
                  : Container(),
              Expanded(
                child: ListView.separated(
                  padding: const EdgeInsets.all(8),
                  itemCount: _user.safePlaces.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      leading: CircleAvatar(
                        backgroundColor: Colors.white,
                        child:
                            Icon(Icons.place_rounded, color: Color(0xfff06400)),
                      ),
                      title: Text('${_user.safePlaces[index].address}'),
                      subtitle: Text('Home'),
                      trailing: Row(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            IconButton(
                              icon: const Icon(Icons.edit),
                              tooltip: 'Edit',
                              onPressed: () => _showMyDialog(
                                  _user.safePlaces[index], _placePicker),
                            ),
                            IconButton(
                              icon: const Icon(Icons.delete),
                              tooltip: 'Delete',
                              onPressed: () => _showMyDialog(
                                  _user.safePlaces[index], _deleteSafePlace),
                            ),
                          ]),
                    );
                  },
                  separatorBuilder: (BuildContext context, int index) =>
                      const Divider(),
                ),
              ),
            ],
          ),
        ),
        Positioned(
          bottom: 20,
          right: 30,
          child: FloatingActionButton(
            heroTag: 'add',
            onPressed: () => _showMyDialog(null, _placePicker),
            tooltip: 'Place Picker',
            backgroundColor: Color(0xff0fF064),
            child: Icon(Icons.add, color: Colors.black),
          ),
        ),
      ]),
      bottomNavigationBar: BottomBar(user: _user),
      floatingActionButton: PanicButton(user: _user),
      resizeToAvoidBottomInset: false,
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }
}
