import 'dart:convert';
import 'package:hive/hive.dart';
import 'package:pref_dessert/pref_dessert.dart';

part 'safePlace.g.dart';

@HiveType(typeId: 1)
class SafePlace extends HiveObject{

  @HiveField(0)
  String address;

  @HiveField(1)
  double latitude;

  @HiveField(2)
  double longitude;
}

class SafePlaceDesSer extends DesSer<SafePlace> {
  @override
  SafePlace deserialize(String s) {
    final Map map = json.decode(s);
    final address = new SafePlace();

    address.address = map['address'];
    address.latitude = map['latitude'];
    address.longitude = map['longitude'];
    return address;
  }

  @override
  String get key => 'ADDRESS';

  @override
  String serialize(SafePlace address) {
    final Map jAddress = {
      'address': address.address,
      'latitude': address.latitude,
      'longitude': address.longitude,
    };
    return json.encode(jAddress);
  }
}
