// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'safeContact.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SafeContactAdapter extends TypeAdapter<SafeContact> {
  @override
  final int typeId = 2;

  @override
  SafeContact read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SafeContact()
      ..name = fields[0] as String
      ..phone = fields[1] as String
      ..avatar = (fields[2] as List)?.cast<dynamic>()
      ..active = fields[3] as bool;
  }

  @override
  void write(BinaryWriter writer, SafeContact obj) {
    writer
      ..writeByte(4)
      ..writeByte(0)
      ..write(obj.name)
      ..writeByte(1)
      ..write(obj.phone)
      ..writeByte(2)
      ..write(obj.avatar)
      ..writeByte(3)
      ..write(obj.active);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SafeContactAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
