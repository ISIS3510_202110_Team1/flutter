import 'dart:convert';

import 'package:pref_dessert/pref_dessert.dart';
import 'package:wibuapp/Model/currentPlace.dart';
import 'package:wibuapp/Model/safePlace.dart';
import 'package:wibuapp/Model/safeContact.dart';
import 'package:hive/hive.dart';

part 'user.g.dart';

@HiveType(typeId: 0)
class User extends HiveObject {
  String id;

  @HiveField(0)
  DateTime birthdate;

  @HiveField(1)
  String email;

  @HiveField(2)
  String firstName;

  @HiveField(3)
  String lastName;

  @HiveField(4)
  String gender;

  @HiveField(5)
  String telephone;

  @HiveField(6)
  String lstActivity;

  @HiveField(7)
  String profileUrl;

  @HiveField(8)
  int taxitimes;

  List<SafePlace> safePlaces = [];

  List<SafeContact> safeContacts = [];

  List<CurrentPlace> currentPlaces = [];

  List<String> userActivity = [];

  void addSafePlace(SafePlace safePlace) {
    safePlaces.add(safePlace);
  }

  void deleteSafePlace(SafePlace safePlace) {
    safePlaces.removeWhere((e) => e.address == safePlace.address);
  }

  void updateSafePlace(SafePlace safePlace, SafePlace data) {
    deleteSafePlace(safePlace);
    addSafePlace(data);
  }

  void addSafeContact(SafeContact safecontact) {
    safeContacts.add(safecontact);
  }

  void deleteSafeContact(SafeContact safeContact) {
    safeContacts.removeWhere((e) => e.phone == safeContact.phone);
  }

  void updateSafeContact(SafeContact safeContact, SafeContact data) {
    deleteSafeContact(safeContact);
    addSafeContact(data);
  }

  void deleteCurrentPlace(CurrentPlace safePlace) {
    currentPlaces.removeWhere((e) => e.address == safePlace.address);
  }

  void updateCurrentPlace(CurrentPlace safePlace, CurrentPlace data) {
    deleteCurrentPlace(safePlace);
    addCurrentPlace(data);
  }

  void addCurrentPlace(CurrentPlace safePlace) {
    final Iterable<CurrentPlace> exist =
        currentPlaces.where((element) => element.address == safePlace.address);
    print('exist.length');
    print(exist.length);
    currentPlaces.add(safePlace);
  }

  void updateLstActivity(String lstActivity) {
    this.lstActivity = lstActivity;
  }

  void addUserACT(String lstActivity) {
    userActivity.add(lstActivity);
  }

  void addUserTaxi() {
    taxitimes += 1;
  }

  Map<String, dynamic> usermap() {
    final Map<String, dynamic> data = {
      'id': id,
      'email': email,
      'firstName': firstName,
      'lastName': lastName,
      'birthdate': birthdate,
      'gender': gender,
      'telephone': telephone,
      'lstActivity': lstActivity,
      'userActivity': userActivity,
      'taxitimes': taxitimes,
    };
    return data;
  }
}

class UserDesSer extends DesSer<User> {
  @override
  User deserialize(String s) {
    final map = json.decode(s);
    final User user = new User();
    user.id = map['id'];
    user.birthdate = map['birthdate'];
    user.email = map['email'];
    user.firstName = map['firstName'];
    user.lastName = map['lastName'];
    user.gender = map['gender'];
    user.telephone = map['telephone'];
    user.lstActivity = map['lstActivity'];
    user.userActivity = map['userActivity'];
    user.profileUrl = map['profileUrl'];

    return user;
  }

  @override
  String get key => 'USER';

  @override
  String serialize(User user) {
    final Map jsonUser = {
      'id': user.id,
      'email': user.email,
      'firstName': user.firstName,
      'lastName': user.lastName,
      'birthdate': user.birthdate,
      'gender': user.gender,
      'telephone': user.telephone,
      'lstActivity': user.lstActivity,
      'userActivity': user.userActivity,
      'profileUrl': user.profileUrl
    };
    return json.encode(jsonUser);
  }
}
