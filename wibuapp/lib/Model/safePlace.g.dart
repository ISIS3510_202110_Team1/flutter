// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'safePlace.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class SafePlaceAdapter extends TypeAdapter<SafePlace> {
  @override
  final int typeId = 1;

  @override
  SafePlace read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return SafePlace()
      ..address = fields[0] as String
      ..latitude = fields[1] as double
      ..longitude = fields[2] as double;
  }

  @override
  void write(BinaryWriter writer, SafePlace obj) {
    writer
      ..writeByte(3)
      ..writeByte(0)
      ..write(obj.address)
      ..writeByte(1)
      ..write(obj.latitude)
      ..writeByte(2)
      ..write(obj.longitude);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is SafePlaceAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
