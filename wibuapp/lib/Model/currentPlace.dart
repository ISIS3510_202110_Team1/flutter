import 'dart:convert';
import 'package:hive/hive.dart';
import 'package:pref_dessert/pref_dessert.dart';
import 'package:wibuapp/Model/safePlace.dart';

part 'currentPlace.g.dart';

@HiveType(typeId: 3)
class CurrentPlace extends HiveObject {
  @HiveField(0)
  String address;

  @HiveField(1)
  double latitude;

  @HiveField(2)
  double longitude;

  @HiveField(3)
  double timesvisited;

  @HiveField(4)
  bool addedTosafeplace;

  @HiveField(5)
  DateTime lstVisited;

  SafePlace toSafePlace() {
    SafePlace s;
    s.address = address;
    s.latitude = latitude;
    s.longitude = longitude;
    return s;
  }
}

class CurrentPlaceDesSer extends DesSer<CurrentPlace> {
  @override
  CurrentPlace deserialize(String s) {
    final Map map = json.decode(s);
    final address = new CurrentPlace();

    address.address = map['address'];
    address.latitude = map['latitude'];
    address.longitude = map['longitude'];
    address.timesvisited = map['timesvisited'];
    address.addedTosafeplace = map['addedTosafeplace'];
    address.lstVisited = map['lstVisited'];
    return address;
  }

  @override
  String get key => 'ADDRESS';

  @override
  String serialize(CurrentPlace address) {
    final Map jAddress = {
      'address': address.address,
      'latitude': address.latitude,
      'longitude': address.longitude,
      'timesvisited': address.timesvisited,
      'addedTosafeplace': address.addedTosafeplace,
      'lstVisited': address.lstVisited,
    };
    return json.encode(jAddress);
  }
}
