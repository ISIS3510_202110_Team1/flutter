import 'dart:convert';
import 'package:hive/hive.dart';
import 'package:pref_dessert/pref_dessert.dart';

part 'safeContact.g.dart';

@HiveType(typeId: 2)
class SafeContact extends HiveObject{

  @HiveField(0)
  String name;

  @HiveField(1)
  String phone;

  @HiveField(2)
  List<dynamic> avatar;

  @HiveField(3)
  bool active;
}

class SafeContactDesSer extends DesSer<SafeContact> {
  @override
  SafeContact deserialize(String s) {
    final Map map = json.decode(s);
    final contact = new SafeContact();

    contact.name = map['name'];
    contact.phone = map['phone'];
    contact.avatar = map['avatar'];
    contact.active = map['active'];
    return contact;
  }

  @override
  String get key => 'ADDRESS';

  @override
  String serialize(SafeContact contact) {
    final Map jContact = {
      'name': contact.name,
      'phone': contact.phone,
      'avatar': contact.avatar,
      'active': contact.active,
    };
    return json.encode(jContact);
  }
}