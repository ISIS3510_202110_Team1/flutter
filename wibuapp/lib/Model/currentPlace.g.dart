// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'currentPlace.dart';

// **************************************************************************
// TypeAdapterGenerator
// **************************************************************************

class CurrentPlaceAdapter extends TypeAdapter<CurrentPlace> {
  @override
  final int typeId = 3;

  @override
  CurrentPlace read(BinaryReader reader) {
    final numOfFields = reader.readByte();
    final fields = <int, dynamic>{
      for (int i = 0; i < numOfFields; i++) reader.readByte(): reader.read(),
    };
    return CurrentPlace()
      ..address = fields[0] as String
      ..latitude = fields[1] as double
      ..longitude = fields[2] as double
      ..timesvisited = fields[3] as double
      ..addedTosafeplace = fields[4] as bool
      ..lstVisited = fields[5] as DateTime;
  }

  @override
  void write(BinaryWriter writer, CurrentPlace obj) {
    writer
      ..writeByte(6)
      ..writeByte(0)
      ..write(obj.address)
      ..writeByte(1)
      ..write(obj.latitude)
      ..writeByte(2)
      ..write(obj.longitude)
      ..writeByte(3)
      ..write(obj.timesvisited)
      ..writeByte(4)
      ..write(obj.addedTosafeplace)
      ..writeByte(5)
      ..write(obj.lstVisited);
  }

  @override
  int get hashCode => typeId.hashCode;

  @override
  bool operator ==(Object other) =>
      identical(this, other) ||
      other is CurrentPlaceAdapter &&
          runtimeType == other.runtimeType &&
          typeId == other.typeId;
}
