import 'package:flutter/material.dart';

Widget approvedMessage(BuildContext context, String message) {
  return SimpleDialog(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.zero,
        child: Column(
          children: <Widget>[
            Icon(Icons.done, size: 100.0, color: Colors.green),
            Text(message, style: Theme.of(context).textTheme.subtitle2),
          ],
        ),
      ),
    ],
  );
}
