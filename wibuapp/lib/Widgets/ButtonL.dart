import 'package:flutter/material.dart';

class ButtonL extends StatelessWidget {
  const ButtonL(
      {@required this.onPressed,
      @required this.color,
      @required this.child,
      @required this.minWidth,
      @required this.height,
      @required this.splashColor});
      
  final Color color;

  final Function onPressed;
  final Widget child;
  final double minWidth;
  final double height;
  final Color splashColor;

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
        minWidth: minWidth,
        height: height,
        color: color,
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
        child: child,
        onPressed: onPressed,
        splashColor: splashColor);
  }
}
