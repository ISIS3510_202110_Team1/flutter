import 'package:flutter/material.dart';

Widget offlineWarningMessage(dismissWarning) {
  return MaterialBanner(
    content: Padding(
      padding: EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Text('Your app is offline'),
          Text(
              'We will sync your data when you recover your internet connection.'),
        ])
      ),
      leading: CircleAvatar(child: Icon(Icons.wifi_off_rounded)),
      actions: [
        TextButton(
          child: const Text('DISMISS'),
          onPressed: () { dismissWarning(); },
        ),
      ],
  );
}
