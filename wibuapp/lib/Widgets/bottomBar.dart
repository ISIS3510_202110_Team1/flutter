import 'package:flutter/material.dart';
import 'package:wibuapp/Model/user.dart';
import 'package:wibuapp/Screens/SafePlaces.dart';
import 'package:wibuapp/Screens/home.dart';
import 'package:wibuapp/Screens/profile.dart';
import 'package:wibuapp/Screens/safeContacts.dart';

class BottomBar extends StatelessWidget {
  const BottomBar({
    @required this.user,
  });
  final User user;

  @override
  Widget build(BuildContext context) {
    return BottomAppBar(
      shape: CircularNotchedRectangle(),
      child: Row(
        children: [
          TextButton(
            onPressed: () => {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => HomePage(user)))
            },
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Icon(Icons.home_outlined),
                Text('Home'),
              ],
            ),
          ),
          TextButton(
            onPressed: () => {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => SafePlacePage(user)))
            },
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Icon(Icons.place_outlined),
                Text('Safe Places'),
              ],
            ),
          ),
          Spacer(),
          TextButton(
            onPressed: () => {
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(
                      builder: (context) => SafeContactPage(user)))
            },
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Icon(Icons.phone_outlined),
                Text('Safe Contacts'),
              ],
            ),
          ),
          TextButton(
            onPressed: () => {
              Navigator.pushReplacement(context,
                  MaterialPageRoute(builder: (context) => ProfilePage(user)))
            },
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Icon(Icons.person_outline),
                Text('Profile'),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
