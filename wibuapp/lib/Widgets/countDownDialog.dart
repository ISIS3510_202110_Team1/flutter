import 'dart:async';
import 'package:flutter/material.dart';

Future<bool> countDownDialog(BuildContext context) async {
  Timer _timer;
  int _counter = 5;

  final StreamController<int> _events = new StreamController<int>();
  _events.add(5);

  _timer = Timer.periodic(Duration(seconds: 1), (timer) {
    if (_counter > 0) {
      _counter--;
      _events.add(_counter);
    } else {
      _timer.cancel();
      Navigator.of(context).pop(false);
    }     
  });

  return showDialog<bool>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Text(' Send panic alert! '),
          content: StreamBuilder<int>(
            stream: _events.stream,
            builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
              print(snapshot.data.toString());
              return CircleAvatar(
                backgroundColor: Color(0xfff06400),
                child: Text(snapshot.data.toString(),
                  style: TextStyle(color: Colors.white),
                ));
            }
          ),
          actions: <Widget>[
            TextButton(
              child: Text('CANCEL'),
              onPressed: () {
                _timer.cancel();
                Navigator.of(context).pop(true);
              },
            ),
          ],
        );
      },
    );
}