import 'package:flutter/material.dart';
import 'package:wibuapp/Controller/NotificationController.dart';
import 'package:wibuapp/Model/user.dart';
import 'package:wibuapp/Services/crudPanicNotification.dart';
import 'package:wibuapp/Widgets/countDownDialog.dart';

class PanicButton extends StatelessWidget {
  PanicButton({
    @required this.user,
  });
  
  final User user;
  final CrudMethodPanicNotifications panicNotificationController = new CrudMethodPanicNotifications();

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 80,
      height: 80,
      child: FloatingActionButton(
          heroTag: 'panic',
          child: CircleAvatar(
            radius: 80,
            backgroundImage: AssetImage(
              'assets/panic-button.png',
            ),
          ),
          onPressed: () async {
            countDownDialog(context).then((cancel) => {
              if (!cancel) {
                sendPanicNotification(user)
              },
              panicNotificationController.addPanicNotification(user, cancel, 'pressing')
            });
          }),
    );
  }
}
