import 'dart:core';
import 'package:flutter/material.dart';

class BuildTextFormField extends StatelessWidget {
  const BuildTextFormField(this.controller, this.validator, this.obscure, this.hint,
      this.label, this.onSaved, this.icon, this.context,
      {Key key})
      : super(key: key);
      
  final TextEditingController controller;
  final FormFieldValidator<String> validator;
  final bool obscure;
  final String hint;
  final String label;
  final FormFieldSetter<String> onSaved;
  final Icon icon;
  final BuildContext context;

  @override
  Widget build(BuildContext context) {
    return Theme(
        data: Theme.of(context).copyWith(primaryColor: Colors.grey),
        child: TextFormField(
          style: TextStyle(
              //color: Colors.white,
              ),
          controller: controller,
          obscureText: obscure,
          validator: validator,
          //maxLength: 100,
          decoration: new InputDecoration(
            isDense: true,
            hintText: hint,
            prefixIcon: icon,
            labelText: label,
            hintStyle: TextStyle(
                //color: Colors.white,
                ),
          ),
          onSaved: onSaved,
        ));
  }
}
