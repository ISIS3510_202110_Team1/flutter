import 'package:flutter/material.dart';

Widget warningMessage(BuildContext context, String message) {
  return SimpleDialog(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.zero,
        child: Column(
          children: <Widget>[
            Icon(Icons.warning, size: 100.0, color: Colors.yellow),
            Text(message, style: Theme.of(context).textTheme.subtitle2),
          ],
        ),
      ),
    ],
  );
}
