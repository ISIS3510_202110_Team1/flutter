import 'package:flutter/material.dart';

Widget errorMessage(BuildContext context, String message) {
  return SimpleDialog(
    children: <Widget>[
      Padding(
        padding: EdgeInsets.zero,
        child: Column(
          children: <Widget>[
            Icon(Icons.error, size: 100.0, color: Colors.red),
            Text(message, style: Theme.of(context).textTheme.subtitle2),
          ],
        ),
      )
    ],
  );
}
