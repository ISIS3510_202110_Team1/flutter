import 'package:flutter/material.dart';

Future<void> connectionFailedMessage(BuildContext context) async {
  return showDialog<void>(
      context: context,
      barrierDismissible: false, // user must tap button!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Column(
          children:[
            Image.asset(
              'assets/sad-owl.png',
              width: 100, height: 100, fit: BoxFit.contain,
            ),
            Text('  Connection failed ')
            ]
          ),
          content: SingleChildScrollView(
            child: ListBody(
              children: <Widget>[
                Text('Please check your internet connection and try again.')
              ],
            ),
          ),
          actions: <Widget>[
            TextButton(
              child: Text('OK'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
}