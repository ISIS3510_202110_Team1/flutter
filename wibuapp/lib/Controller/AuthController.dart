import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:pref_dessert/pref_dessert.dart';
import 'package:wibuapp/Services/crudSafeContact.dart';
import 'package:wibuapp/Services/crudSafePlace.dart';
import 'package:wibuapp/Services/crudUser.dart';

import 'package:wibuapp/Model/user.dart' as u;

class AuthController {
  //UserCredential _user;
  FirebaseUser _user;

  final FirebaseAuth _auth = FirebaseAuth.instance;
  bool isLoggedin() {
    return _auth.currentUser != null ? true : false;
  }

  Future<FirebaseUser> getUser() async {
    return _auth.currentUser();
  }

  Future<u.User> login(String email, String password) async {
    _user = await _auth.signInWithEmailAndPassword(
        email: email, password: password);

    final CrudMethodsUser userController = new CrudMethodsUser();
    final u.User user = await userController.setUser(_user.uid);
    return user;
  }

  Future<FirebaseUser> createUser(String email, String password) async {
    _user = await _auth.createUserWithEmailAndPassword(
        email: email, password: password);

    return _user;
  }

  Future<void> passworddResetEmail(String email) async {
    await _auth.sendPasswordResetEmail(email: email);
  }

  Future logOut() async {
    FirebaseAuth.instance.signOut();
    _user = null;
    final repo = new FuturePreferencesRepository<u.User>(new u.UserDesSer());
    final CrudMethodsUser cruduser = new CrudMethodsUser();
    final CrudMethodSafePlaces crudplaces = new CrudMethodSafePlaces();

    final CrudMethodSafeContacts crudcontacts = new CrudMethodSafeContacts();
    await cruduser.clearUserLocalStorage();
    await crudplaces.clearSafePlaceLocalStorage();
    await crudcontacts.clearSafeContactLocalStorage();

    repo.removeAll();
  }
}
