import 'dart:async';
import 'dart:io';
import 'package:firebase_storage/firebase_storage.dart';

class FirebStorage {
  Future<String> send(imagen, rutaGuardar) async {
    final StorageReference reference =
        FirebaseStorage.instance.ref().child(rutaGuardar);

    final File file = File(imagen);
    final StorageUploadTask uploadTask = reference.putFile(file);
    final StorageTaskSnapshot storageTaskSnapshot =
        await uploadTask.onComplete.then((value) {
      return value;
    });

    final String url = await storageTaskSnapshot.ref.getDownloadURL();
    print(url);
    return url;
  }
}
