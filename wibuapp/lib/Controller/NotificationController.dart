import 'dart:async';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:geolocator/geolocator.dart';
import 'package:location/location.dart';
import 'package:connectivity/connectivity.dart';
import 'package:telephony/telephony.dart';

import 'package:wibuapp/Controller/AuthController.dart';
import 'package:wibuapp/Controller/CurrentPlacesController.dart';
import 'package:wibuapp/Controller/LocationController.dart';
import 'package:wibuapp/Model/safePlace.dart';
import 'package:wibuapp/Model/safeContact.dart';
import 'package:wibuapp/Model/user.dart';
import 'package:wibuapp/Services/crudSafeContact.dart';

Future<void> sendLicensePlate(User user, String licensePlate, String from,
    String brand, String transportationType) async {
  final Position currentLocation = await getCurrentLocation();
  const String title = 'Transport Notification';
  String message = '${user.firstName} is in a $transportationType vehicle';
  if (brand != null) {
    message += ' of $brand brand';
  }
  if (licensePlate != null) {
    message += '. It license plate is $licensePlate';
    if (from != null) {
      message += ' from $from';
    }
  } else {
    if (from != null) {
      message += '. It license plate is from $from';
    }
  }
  message += '. ${user.firstName} current location is Latitude:${currentLocation.latitude} Longitude:${currentLocation.longitude}';
  sendNotifications(title, message);
}

Future<void> sendPanicNotification(User user) async {
  final Position currentLocation = await getCurrentLocation();
  const String title = 'Panic alert!';
  final String message =
      '${user.firstName} is in danger Latitude:${currentLocation.latitude} Longitude:${currentLocation.longitude}';
  sendNotifications(title, message);
}

Future<void> sendSafePlaceNotification(User user, bool inSafePlace) async {
  String title = 'Not in safe place';
  String message = '${user.firstName} is no more in a safe place';
  if (inSafePlace) {
    title = 'In safe place';
    message = '${user.firstName} is in a safe place';
  }
  sendNotifications(title, message);
}

Future<void> sendNotifications(String title, String message) async {
  final CrudMethodSafeContacts safeContactController = new CrudMethodSafeContacts();
  final List<SafeContact> safeContacts = await safeContactController
      .getSafeContactsLocalStorage(); // Read from local storage
  print('safeContacts.length');
  print(safeContacts.length);
  final ConnectivityResult connectivityResult =
      await Connectivity().checkConnectivity();
  for (var safeContact in safeContacts) {
    print(safeContact.active);
    if (safeContact.active) {
      if (connectivityResult == ConnectivityResult.none) {
        Telephony.instance.sendSms(to: safeContact.phone, message: message);
      } else {
        print('t');
        Telephony.instance.sendSms(to: safeContact.phone, message: message);
        final results = await CloudFunctions.instance.call(
            functionName: 'sendNotification',
            parameters: <String, dynamic>{
              'title': title,
              'body': message,
              'user': safeContact.phone
            });
        print(results);
      }
    }
  }
}

class PushNotificationsHandler {
  factory PushNotificationsHandler() => _instance;
  PushNotificationsHandler._();
  static final PushNotificationsHandler _instance =
      PushNotificationsHandler._();
  final FirebaseMessaging _firebaseMessaging = FirebaseMessaging();
  bool _initialized = false;

  Future<void> init(String telephone) async {
    if (!_initialized) {
      // For iOS request permission first.
      _firebaseMessaging.requestNotificationPermissions();
      //You can subscribed to a topic, if you need to send to all devices
      //user segmented messages is not supported for the Admin SDK
      _firebaseMessaging.subscribeToTopic(telephone);
      print(telephone);
      _firebaseMessaging.configure(
        //fires when the app is open and running in the foreground.
        onMessage: (Map<String, dynamic> message) async {
          print('onMessage: $message');
          //do whatever
        },

        //fires if the app is fully terminated.
        onLaunch: (Map<String, dynamic> message) async {
          print('onLaunch: $message');
          //do whatever
        },
        //fires if the app is closed, but still running in the background.
        onResume: (Map<String, dynamic> message) async {
          print('onResume: $message');
          //do whatever
        },
      );
      // For testing purposes print the Firebase Messaging token
      final String token = await _firebaseMessaging.getToken();
      print('FirebaseMessaging token: $token');
      _initialized = true;
    }
  }
}

Future<void> backgroundLocation(User user) async {
  final Location location = new Location();

  List<SafePlace> safePlaces = user
      .safePlaces; //await safePlaceController.getSafePlacesLocalStorage(); // Read from local storage
  // location.enableBackgroundMode(enable: true);

  // Address safePlace = new Address('Mi Casa', 10.3906896, -75.4730897);
  print('safePlaces.length');
  print(safePlaces.length);
  bool inSafePlace = true;

  location.onLocationChanged.listen((LocationData currentLocation) async {
    if (new AuthController().isLoggedin()) {
      safePlaces = user
          .safePlaces; //await safePlaceController.getSafePlacesLocalStorage();
      print('safePlaces.length');
      print(safePlaces.length);
    }

    // print('Change location' + safePlaces.length.toString());
    double minDistance = double.maxFinite;
    for (var safePlace in safePlaces) {
      final double distance = Geolocator.distanceBetween(
          safePlace.latitude,
          safePlace.longitude,
          currentLocation.latitude,
          currentLocation.longitude);
      if (distance < minDistance) {
        minDistance = distance;
      }
    }
    print('Min Distance ' + minDistance.toString());
    print(currentLocation.latitude);

    if (minDistance > 200 && inSafePlace) {
      inSafePlace = false;
      print('sent');

      sendSafePlaceNotification(user, inSafePlace);
    } else if (minDistance <= 200 && !inSafePlace) {
      inSafePlace = true;
      sendSafePlaceNotification(user, inSafePlace);
      print('sent');
    }
    //print(user.lstActivity);

    user = await addCurrentPlace(
        currentLocation.latitude, currentLocation.longitude, user);
  }).onData((currentLocation) async {
    if (new AuthController().isLoggedin() && user != null) {
      safePlaces = user
          .safePlaces; //await safePlaceController.getSafePlacesLocalStorage();
      print('safePlaces.length');
      print(safePlaces.length);
    }

    // print('Change location' + safePlaces.length.toString());
    double minDistance = double.maxFinite;
    for (var safePlace in safePlaces) {
      final double distance = Geolocator.distanceBetween(
          safePlace.latitude,
          safePlace.longitude,
          currentLocation.latitude,
          currentLocation.longitude);
      if (distance < minDistance) {
        minDistance = distance;
      }
    }
    print('Min Distance' + minDistance.toString());
    print(currentLocation.latitude);

    if (minDistance > 200 && inSafePlace) {
      inSafePlace = false;
      sendSafePlaceNotification(user, inSafePlace);
    } else if (minDistance <= 200 && !inSafePlace) {
      inSafePlace = true;
      sendSafePlaceNotification(user, inSafePlace);
    }
    //print(user.lstActivity);
    if (new AuthController().isLoggedin() && user != null) {
      user = await addCurrentPlace(
          currentLocation.latitude, currentLocation.longitude, user);
    }
  });
}
