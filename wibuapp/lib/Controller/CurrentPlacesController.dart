import 'package:connectivity/connectivity.dart';
import 'package:geolocator/geolocator.dart';
import 'package:geocoder/geocoder.dart';
import 'package:wibuapp/Model/currentPlace.dart';
import 'package:wibuapp/Model/user.dart';
import 'package:wibuapp/Services/crudCurrentPlace.dart';
import 'package:activity_recognition_flutter/activity_recognition_flutter.dart';

double distance(double plac1Longitud, double plac1Latitude,
    double plac2Longitud, double plac2Latitud) {
  final double distance = Geolocator.distanceBetween(
      plac1Latitude, plac1Longitud, plac2Latitud, plac2Longitud);
  return distance;
}

bool isSafePlace(latitude, longitude, User user) {
  // print('Change location' + safePlaces.length.toString());
  double minDistance = double.maxFinite;
  bool isSafePlace = false;

  for (var safePlace in user.safePlaces) {
    final double d =
        distance(safePlace.longitude, safePlace.latitude, longitude, latitude);
    if (d < minDistance) {
      minDistance = d;
    }
  }

  if (minDistance < 200) {
    isSafePlace = true;
  }
  return isSafePlace;
}

Future<User> addCurrentPlace(latitude, longitude, User user) async {
  bool found = false;
  final bool safePlaceis = isSafePlace(latitude, longitude, user);

  final CrudMethodCurrentPlaces ccp = new CrudMethodCurrentPlaces();
//  List<CurrentPlace> safePlaces = await ccp.getCurrentPlaceLocalStorage();
  double minDistance = double.maxFinite;

  if (!safePlaceis) {
    CurrentPlace closeSafePlace = new CurrentPlace();
    for (var safePlace in user.currentPlaces) {
      final double d = distance(
          safePlace.longitude, safePlace.latitude, longitude, latitude);

      if (d < minDistance && d < 500) {
        minDistance = d;

        found = true;
        closeSafePlace = safePlace;
      }
    }

    final ConnectivityResult connectivityResult =
        await Connectivity().checkConnectivity();
    final DateTime today = DateTime.now();
    int since;
    if (found && minDistance < 1000) {
      if (closeSafePlace.lstVisited != null) {
        since = closeSafePlace.lstVisited.difference(today).inMinutes.abs();
      } else {
        since = 0;
      }
      final CurrentPlace newData = new CurrentPlace();

      newData.address = closeSafePlace.address;
      newData.latitude = closeSafePlace.latitude;
      newData.longitude = closeSafePlace.longitude;
      newData.lstVisited = DateTime.now();
      newData.timesvisited = closeSafePlace.timesvisited + 1;
      newData.addedTosafeplace = closeSafePlace.addedTosafeplace;
      if (connectivityResult != ConnectivityResult.none && since > 3) {
        user = await ccp.updateCurrentPlace(user, closeSafePlace, newData);
      }
    } else if (!found) {
      final String address = await getUserLocation(latitude, longitude);

      final CurrentPlace newCurrent = new CurrentPlace();

      newCurrent.address = address;
      newCurrent.latitude = latitude;
      newCurrent.longitude = longitude;
      newCurrent.timesvisited = 1;
      newCurrent.addedTosafeplace = false;
      newCurrent.lstVisited = DateTime.now();

      if (connectivityResult != ConnectivityResult.none) {
        user = await ccp.addCurrentPlace(user, newCurrent);
        print('added');
      }
    } else {
      print('no no no ');
    }
  }

  return user;
}

Future<String> getUserLocation(latitude, longitud) async {
  //call this async method from whereever you need
  print('latitude');

  print(latitude);
  print('longitud');
  print(longitud);

  final coordinates = new Coordinates(latitude, longitud);
  final addresses =
      await Geocoder.local.findAddressesFromCoordinates(coordinates);
  final first = addresses.first;

  return first.addressLine;
}

Future<void> activity(User user) async {
  final Stream<Activity> activities = ActivityRecognition.activityUpdates();

  activities.listen((f) {
    print(f.type.toString());
  }).onData((currentActivity) {
    print(currentActivity.type.toString());
  });
}
