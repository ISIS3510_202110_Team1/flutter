import 'dart:async';
import 'dart:math';
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:speech_to_text/speech_recognition_error.dart';
import 'package:speech_to_text/speech_recognition_result.dart';
import 'package:speech_to_text/speech_to_text.dart';

import 'package:wibuapp/Controller/NotificationController.dart';
import 'package:wibuapp/Model/user.dart';
import 'package:wibuapp/Services/crudPanicNotification.dart';
import 'package:wibuapp/Widgets/countDownDialog.dart';

class SpeechToTextController {
  SpeechToTextController(User user, BuildContext context) {
    this.user = user;
    this.context = context;
    initSpeechState();   
  }
  final CrudMethodPanicNotifications panicNotificationController =
    new CrudMethodPanicNotifications();
  User user;
  BuildContext context;
  double level = 0.0;
  double minSoundLevel = 50000;
  double maxSoundLevel = -50000;
  String lastWords = '';
  String lastError = '';
  String lastStatus = '';
  String _currentLocaleId = '';
  final SpeechToText speech = SpeechToText();


  void start() {
    Timer.periodic(Duration(seconds: 20), (Timer t) => {
      Connectivity().checkConnectivity().then((connectivityResult) {
        if (connectivityResult != ConnectivityResult.none) {
          startListening();
        }
      }) 
    });
  }


  Future<void> initSpeechState() async {
    final hasSpeech = await speech.initialize(
        onError: errorListener,
        onStatus: statusListener,
        debugLogging: true,
        finalTimeout: Duration(milliseconds: 0));
    if (hasSpeech) {

      final systemLocale = await speech.systemLocale();
      _currentLocaleId = systemLocale.localeId;
    }

    start();
  }

  void startListening() {
    lastWords = '';
    lastError = '';
    speech.listen(
        onResult: resultListener,
        listenFor: Duration(seconds: 10),
        pauseFor: Duration(seconds: 10),
        partialResults: false,
        localeId: _currentLocaleId,
        onSoundLevelChange: soundLevelListener,
        cancelOnError: false,
        listenMode: ListenMode.confirmation);
  }

  void stopListening() {
    speech.stop();
    level = 0.0;
  }

  void cancelListening() {
    speech.cancel();
    level = 0.0;
  }

  void resultListener(SpeechRecognitionResult result) {
    lastWords = '${result.recognizedWords} - ${result.finalResult}';
    print(lastWords);
    if (result.recognizedWords.contains('help')) {
      countDownDialog(context).then((cancel) => {
        if (!cancel) {
          sendPanicNotification(user)
        },
        panicNotificationController.addPanicNotification(
            user, cancel, 'voice command')
      });
    }
  }

  void soundLevelListener(double level) {
    minSoundLevel = min(minSoundLevel, level);
    maxSoundLevel = max(maxSoundLevel, level);
    level = level;
  }

  void errorListener(SpeechRecognitionError error) {
    lastError = '${error.errorMsg} - ${error.permanent}';
  }

  void statusListener(String status) {
    lastStatus = '$status';
  }
}