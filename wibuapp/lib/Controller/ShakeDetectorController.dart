import 'package:flutter/material.dart';
import 'package:shake/shake.dart';

import '../Model/user.dart';
import '../Services/crudPanicNotification.dart';
import '../Widgets/countDownDialog.dart';
import 'NotificationController.dart';

class ShakeDetectorController {
  ShakeDetectorController(User user, BuildContext context) {
    this.user = user;
    this.context = context;
  }
  final CrudMethodPanicNotifications panicNotificationController =
      new CrudMethodPanicNotifications();
  final Stopwatch stopwatch = Stopwatch();
  User user;
  BuildContext context;
  ShakeDetector detector;

  void start() {
    detector = ShakeDetector.autoStart(
        onPhoneShake: () {
          if (!stopwatch.isRunning || stopwatch.elapsed.inMinutes >= 1) {
            stopwatch.isRunning ? stopwatch.reset() : stopwatch.start();
            countDownDialog(context).then((cancel) => {
                  if (!cancel)
                    {sendPanicNotification(user)}
                  else
                    {stopwatch.stop()},
                  panicNotificationController.addPanicNotification(
                      user, cancel, 'shaking')
                });
          }
        },
        shakeThresholdGravity: 4.0);
  }

  void stop() {
    detector.stopListening();
    stopwatch.stop();
  }
}
